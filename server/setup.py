#!/usr/bin/python

from setuptools import setup, find_packages, Extension

setup(
  name="djrandom",
  version="0.1",
  description="DJ:Random server",
  author="ale",
  author_email="ale@incal.net",
  url="http://git.autistici.org/git/djrandom.git",
  install_requires=["Flask", "Flask-WTF", "gevent", "eyeD3", "solrpy"],
  setup_requires=[],
  zip_safe=False,
  packages=find_packages(),
  entry_points={
    "console_scripts": [
      "djrandom-receiver = djrandom.receiver.receiver:main",
      "djrandom-scanner = djrandom.scanner.scanner:main",
      "djrandom-fingerprinter = djrandom.fingerprint.fingerprint:main",
      "djrandom-dedup = djrandom.fingerprint.cheap_dedup:main",
      "djrandom-streamer = djrandom.stream.stream:main",
      "djrandom-frontend = djrandom.frontend.frontend:main",
      "djrandom-update-markov = djrandom.model.markov:main",
      "djrandom-metadata-fixer = djrandom.metadata_fixer.metadata_fixer:main",
      "djrandom-solr-fixer = djrandom.model.verify:main",
    ],
  },
  )

