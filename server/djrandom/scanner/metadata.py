import eyeD3
import unicodedata
import re


# Compiling and including 'unicodedata' apparently makes it possible
# to correctly interpret accented characters in unicode tag metadata.
_nonalpha_pattern = re.compile(r'\W+', re.UNICODE)
_spaces_pattern = re.compile(r'\s+', re.UNICODE)

def normalize_string(s):
    if s:
        s = s.replace('_', ' ')
        s = _nonalpha_pattern.sub(' ', s)
        s = _spaces_pattern.sub(' ', s)
        s = s.lower().strip()
    return s


def analyze_mp3(path):
    tag = eyeD3.Tag()
    tag.link(path)
    try:
        genre = tag.getGenre().getName()
    except:
        genre = '' 
    return {'artist': normalize_string(tag.getArtist()),
            'album': normalize_string(tag.getAlbum()),
            'title': normalize_string(tag.getTitle()),
            'track_num': tag.getTrackNum()[0],
            'genre': unicode(genre)}
