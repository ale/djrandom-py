import os
import optparse
import logging
import time
import traceback
from djrandom import daemonize
from djrandom import utils
from djrandom.model.mp3 import MP3
from djrandom.database import Session, init_db, indexer
from djrandom.scanner import metadata
from djrandom.model import processor

log = logging.getLogger(__name__)


class BadMetadataError(Exception):
    pass


class Scanner(processor.Processor):

    def _process(self, mp3):
        mp3_info = metadata.analyze_mp3(mp3.path)
        if not mp3_info.get('artist') or not mp3_info.get('title'):
            raise BadMetadataError()
        for key, value in mp3_info.iteritems():
            setattr(mp3, key, value)

    def query(self):
        return MP3.query.filter_by(state=MP3.INCOMING)

    def process(self, mp3):
        """Scan the database for new files."""
        log.info('processing %s' % mp3.sha1)
        try:
            self._process(mp3)
            mp3.state = MP3.READY
        except BadMetadataError:
            log.info('bad metadata for %s' % mp3.sha1)
            mp3.state = MP3.BAD_METADATA
        except Exception, e:
            log.error(traceback.format_exc())
            mp3.state = MP3.ERROR
        indexer.add_mp3(mp3)

    def commit(self):
        indexer.commit()


def run_scanner(solr_url, db_url, run_once):
    init_db(db_url, solr_url)
    scanner = Scanner()
    scanner.loop(run_once=run_once)


def main():
    parser = optparse.OptionParser()
    parser.add_option('--once', action='store_true')
    parser.add_option('--solr_url', default='http://localhost:8080/solr')
    parser.add_option('--db_url')
    daemonize.add_standard_options(parser)
    utils.read_config_defaults(
        parser, os.getenv('DJRANDOM_CONF', '/etc/djrandom.conf'))
    opts, args = parser.parse_args()
    if not opts.db_url:
        parser.error('Must provide --db_url')
    if args:
        parser.error('Too many arguments')

    if opts.once:
        opts.foreground = True

    daemonize.daemonize(opts, run_scanner,
                        (opts.solr_url, opts.db_url, opts.once))


if __name__ == '__main__':
    main()
