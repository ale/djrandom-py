import hashlib
import os

PATH_NESTING = 2


def generate_path(base_dir, sha1):
    dir_parts = [base_dir]
    dir_parts.extend(sha1[:PATH_NESTING])
    base_path = os.path.join(*dir_parts)
    if not os.path.isdir(base_path):
        os.makedirs(base_path)
    return os.path.join(base_path, sha1)


def random_token():
    return hashlib.sha1(os.urandom(20)).hexdigest()


def sha1_of_file(path):
    with open(path, 'r') as fd:
        sha = hashlib.sha1()
        while True:
            chunk = fd.read(4096)
            if not chunk:
                break
            sha.update(chunk)
        return sha.hexdigest()


class SyntaxError(Exception):
    pass


def read_config_defaults(parser, path):
    if not os.path.exists(path):
        return
    with open(path, 'r') as fd:
        for linenum, line in enumerate(fd):
            line = line.strip()
            if not line or line.startswith('#'):
                continue
            if '=' not in line:
                raise SyntaxError('%s, line %d: Syntax Error' % (
                        path, 1 + linenum))
            var, value = map(lambda x: x.strip(), line.split('=', 1))
            parser.set_default(var, value)
