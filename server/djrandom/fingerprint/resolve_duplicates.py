import logging
from djrandom.database import Session, indexer
from djrandom.model.mp3 import MP3
from djrandom.fingerprint.compare_songs import sort_songs

log = logging.getLogger(__name__)


class Resolver(object):

    def __init__(self):
        self._to_remove = {}
        self._cache = {}

    def resolve_dupes(self, hashes):
        """Perform best duplicate selection and remove dupes from db."""
        hashes_key = ','.join(sorted(hashes))
        log.debug('remove_dupes(%s)' % hashes_key)
        if hashes_key in self._cache:
            return self._cache[hashes_key]

        # Compute 'score' for each song and sort them.
        by_score = sort_songs(hashes=hashes)
        best_song = by_score[0][1]
        log.debug('remove_dupes: best song is %s' % best_song)
        log.debug('remove_dupes: score dump:')
        for score, sha1 in by_score:
            bitrate, duration, nmeta = score
            log.debug(' * (bitrate=%s, duration=%s, nmeta=%s) %s' % (
                    bitrate, duration, nmeta, sha1))

        # Remove all the other songs.
        songs_to_remove = dict((x, best_song) for x in hashes if x != best_song)
        log.info('remove_dupes: songs to remove: %s' % str(songs_to_remove.keys()))
        self._to_remove.update(songs_to_remove)
        self._cache[hashes_key] = best_song
        return best_song

    def commit(self):
        for sha1, duplicate_of in self._to_remove.iteritems():
            mp3 = MP3.query.get(sha1)
            mp3.mark_as_duplicate(duplicate_of)
            Session.add(mp3)
            indexer.add_mp3(mp3)
        Session.commit()
        indexer.commit()

