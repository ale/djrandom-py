"""Deduplicator that uses perfect fingerprint-matching (fast, cheap)."""

import base64
import os
import optparse
import logging
import json
import sys
import time
import traceback
from djrandom import daemonize
from djrandom import utils
from djrandom.database import init_db, Session
from djrandom.model.mp3 import MP3, Fingerprint
from djrandom.fingerprint.resolve_duplicates import Resolver
from sqlalchemy import *

log = logging.getLogger(__name__)


class CheapDeDuper(object):
    """Will find _identical_ duplicates (same bitrate)."""

    def __init__(self, filter_artist=None, filter_title=None):
        self._resolver = Resolver()
        self._filter_artist = filter_artist
        self._filter_title = filter_title

    def _get_query(self):
        qargs = ((MP3.state == MP3.READY)
                  & (MP3.has_fingerprint == True))
        if self._filter_artist:
            qargs = qargs & (MP3.artist == self._filter_artist)
        if self._filter_title:
            qargs = qargs & (MP3.title == self._filter_title)
        return qargs

    def dedupe_fp(self, engine):
        count, errs = 0, 0
        codes = {}
        print 'loading all fingerprints'

        # Skip the ORM and directly query the SQL layer.
        qargs = self._get_query() & (MP3.sha1 == Fingerprint.sha1)
        print 'query:', qargs
        q = select([Fingerprint.sha1, Fingerprint.echoprint_fp], qargs)
        for row in engine.execute(q):
            try:
                enc_code = str(json.loads(row.echoprint_fp)['code'])
                code = base64.urlsafe_b64decode(enc_code)
                if code:
                    codes.setdefault(code, []).append(row.sha1)
            except KeyError:
                continue
            except Exception, e:
                traceback.print_exc()
                errs += 1
                continue
            count += 1
            if count % 100 == 0:
                sys.stdout.write('%d  \r' % count)
                sys.stdout.flush()

        print '\n%d fingerprints, %d errors' % (count, errs)
        Session.remove()

        print 'done, scanning for duplicates'
        for code, hashes in codes.iteritems():
            print '%s -> %s' % (repr(code[:64]), hashes)
            if len(hashes) > 1:
                self._dedup_songs([MP3.query.get(x) for x in hashes])

    def dedupe(self):
        dupes = []
        count, deduped, dedup_dstcount = 0, 0, 0
        last_mp3 = None
        mp3s = MP3.query.filter(self._get_query()
            ).order_by(asc(MP3.artist), asc(MP3.title))
        for mp3 in mp3s:
            count += 1
            if last_mp3:
                if (mp3.artist == last_mp3.artist and
                    mp3.title == last_mp3.title):
                    dupes.append(mp3)
                else:
                    if len(dupes) > 1:
                        deduped += len(dupes) - 1
                        dedup_dstcount += 1
                        self._dedup_songs(dupes)
                    dupes = []
            last_mp3 = mp3
        if len(dupes) > 1:
            deduped += len(dupes) - 1
            dedup_dstcount += 1
            self._dedup_songs(dupes)
        print 'dedup stats: total: %d songs, removed as duplicates: %d, duplicate songs: %d' % (
                 count, deduped, dedup_dstcount)

    def _dedup_songs(self, songs):
        def _toutf8(x):
            try:
                return x.encode('utf-8')
            except:
                return '???'
        print 'dedup group:'
        for s in songs:
            fp = json.loads(s.get_fingerprint()).get('code')
            print '   - %s / %s / %s' % (_toutf8(s.artist), _toutf8(s.title), _toutf8(s.album))
            #print '     [%s]' % str(fp)[:128]
            print '     [%s]' % s.sha1
        best = self._resolver.resolve_dupes([s.sha1 for s in songs])
        print '\n  * best: %s\n' % (best, )

    def commit(self):
        self._resolver.commit()


def run_cheap_deduper(db_url, solr_url, dry_run, use_fp, filter_artist, filter_title):
    engine = init_db(db_url, solr_url)
    dup = CheapDeDuper(filter_artist, filter_title)
    if use_fp:
        dup.dedupe_fp(engine)
    else:
        dup.dedupe()
    if not dry_run:
        dup.commit()


def main():
    parser = optparse.OptionParser()
    parser.add_option('--db_url')
    parser.add_option('--solr_url', default='http://localhost:8080/solr')
    parser.add_option('--apply', action='store_true')
    parser.add_option('--use_meta', action='store_true')
    parser.add_option('--filter_artist')
    parser.add_option('--filter_title')
    daemonize.add_standard_options(parser)
    utils.read_config_defaults(
        parser, os.getenv('DJRANDOM_CONF', '/etc/djrandom.conf'))
    opts, args = parser.parse_args()
    if not opts.db_url:
        parser.error('Must provide --db_url')
    if args:
        parser.error('Too many arguments')

    daemonize.daemonize(opts, run_cheap_deduper,
                        (opts.db_url, opts.solr_url, not opts.apply, not opts.use_meta,
                         opts.filter_artist, opts.filter_title))


if __name__ == '__main__':
    main()
