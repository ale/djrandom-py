import os
import optparse
import logging
import subprocess
import time
import traceback
from djrandom import daemonize
from djrandom import utils
from djrandom.model.mp3 import MP3
from djrandom.database import Session, init_db
from djrandom.model import processor

log = logging.getLogger(__name__)


class Fingerprinter(processor.Processor):

    def __init__(self, codegen_path):
        processor.Processor.__init__(self)
        self.codegen_path = codegen_path

    def process(self, mp3):
        log.info('fingerprinting %s' % mp3.sha1)
        pipe = subprocess.Popen(
            [self.codegen_path, mp3.path, '10', '30'],
            close_fds=False,
            stdout=subprocess.PIPE)
        fp_json = pipe.communicate()[0]
        if fp_json:
            # Remove the square brackets that make fp_json an array.
            # (Ugly Hack!)
            mp3.set_fingerprint(fp_json[2:-2])

    def query(self):
        return MP3.get_with_no_fingerprint()


def run_fingerprinter(db_url, codegen_path, run_once):
    init_db(db_url)
    scanner = Fingerprinter(codegen_path)
    scanner.loop(run_once=run_once)


def main():
    parser = optparse.OptionParser()
    parser.add_option('--once', action='store_true')
    parser.add_option('--codegen_path',
                      default='/usr/local/bin/echoprint-codegen')
    parser.add_option('--db_url')
    daemonize.add_standard_options(parser)
    utils.read_config_defaults(
        parser, os.getenv('DJRANDOM_CONF', '/etc/djrandom.conf'))
    opts, args = parser.parse_args()
    if not opts.db_url:
        parser.error('Must provide --db_url')
    if args:
        parser.error('Too many arguments')

    if opts.once:
        opts.foreground = True

    daemonize.daemonize(opts, run_fingerprinter,
                        (opts.db_url, opts.codegen_path, opts.once))


if __name__ == '__main__':
    main()
