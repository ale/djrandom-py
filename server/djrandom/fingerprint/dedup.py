"""Deduplicator that uses a standalone Echoprint server instance."""

import eyeD3
import fp
import os
import optparse
import logging
import json
import sys
import time
from sqlalchemy import select
from djrandom import daemonize
from djrandom import utils
from djrandom.model.mp3 import MP3, Fingerprint
from djrandom.database import Session, init_db
from djrandom.fingerprint.resolve_duplicates import Resolver

log = logging.getLogger(__name__)


class DeDuper(object):

    def __init__(self, engine, filter_artist=None, filter_title=None):
        self._engine = engine
        self._filter_artist = filter_artist
        self._filter_title = filter_title
        self._resolver = Resolver()

    def _generate_code_json(self, echoprint_fp, sha1):
        """Parse the JSON string output of echoprint-codegen, and return
        a structure that fp.ingest() can deal with.

        Taken from 'fastingest.py', with minor changes.
        """
        c = json.loads(echoprint_fp)
        if 'code' not in c:
            return None

        decoded = fp.decode_code_string(c['code'])
        m = c['metadata']
        data = {'track_id': sha1,
                'fp': decoded,
                'length': m['duration'],
                'codever': '%.2f' % m['version']}
        return data

    def dedupe(self, dry_run=True):
        self._ingest()
        self._scan_for_dupes()
        if not dry_run:
            self._cleanup()

    def _ingest(self):
        self.codes = {}
        """Load all known fingerprints into the db.

        Creates the {sha1: code_string} self.codes dictionary.
        """
        log.debug('loading in-memory fingerprint database...')
        start = time.time()
        fp.erase_database(local=True, really_delete=True)
        # Skip the ORM and directly query the SQL layer.
        qargs = ((MP3.sha1 == Fingerprint.sha1)
                  & (MP3.state == MP3.READY)
                  & (MP3.has_fingerprint == True))
        if self._filter_artist:
            qargs = qargs & (MP3.artist == self._filter_artist)
        if self._filter_title:
            qargs = qargs & (MP3.title == self._filter_title)
        log.debug('query: %s', qargs)
        q = select([Fingerprint.sha1, Fingerprint.echoprint_fp], qargs)
        count = 0
        for row in self._engine.execute(q):
            count += 1
            if (count % 100) == 0:
                sys.stderr.write('%d       \r' % count)
                sys.stderr.flush()
            code = self._generate_code_json(row.echoprint_fp, row.sha1)
            if not code:
                continue
            self.codes[row.sha1] = code['fp']
            fp.ingest(code, do_commit=False, local=True)
        elapsed = time.time() - start
        log.debug('loaded in-memory fingerprint database in %g seconds, '
                  '%d fingerprints', elapsed, count)

    def _scan_for_dupes(self):
        # Now dedupe by going through all our codes over again.
        log.debug('de-duping fingerprint database (%d codes)...', len(self.codes))
        start = time.time()
        dup_count = 0
        for sha1, code in self.codes.iteritems():
            results = fp.query_fp(code, local=True).results
            if len(results) < 2:
                continue
            if self._dedupe_song(sha1, code, results):
                dup_count += 1
        elapsed = time.time() - start
        log.debug('de-duped fingerprint database in %g seconds' % elapsed)
        log.debug('found %d duplicates' % dup_count)
        return dup_count

    def _dedupe_song(self, sha1, code_string, results):
        """Find fingerprint matches and eventually de-duplicate a song.
        
        Returns True if de-duplication was performed, False otherwise.
        """
        elbow = 10
        code_len = len(code_string.split(' ')) / 2
        actual_scores = {}
        original_scores = {}
        for entry in results:
            track_id = entry['track_id']
            track_code = fp.local_fp_code_for_track_id(track_id)
            actual_scores[track_id] = fp.actual_matches(
                code_string, track_code, elbow=elbow)
            original_scores[track_id] = entry['score']

        # Histogram-based score computation. Only keep the highest per-track score.
        sorted_actual_scores = sorted(actual_scores.iteritems(),
                                      key=lambda (k, v): v, reverse=True)
        new_sorted_actual_scores = []
        existing_track_ids = set()
        for trid, score in sorted_actual_scores:
            track_id = trid.split('-')[0]
            if track_id not in existing_track_ids:
                existing_track_ids.add(track_id)
                new_sorted_actual_scores.append((trid, score))

        dupes = []

        top_score = new_sorted_actual_scores[0][1]
        for track_id, score in new_sorted_actual_scores:
            track_sha1 = track_id.split('-')[0]
            if score < code_len * 0.1:
                continue
            if score < top_score / 2:
                continue
            #if (top_score - score) < (top_score / 2):
            #    continue
            dupes.append((track_sha1, score, original_scores[track_id]))

        if len(dupes) < 2:
            # Only one fingerprint matches. Good!
            return False

        # Print out some debugging information.
        orig = MP3.query.get(sha1)
        log.info("duplicates for '%s/%s' (%s) code_len=%d:" % (
                orig.artist, orig.title, sha1, code_len))
        for track_sha1, score, original_score in dupes:
            mp3 = MP3.query.get(track_sha1)
            log.info('  --> %s (%s orig:%s), %s/%s' % (
                    track_sha1, score, original_score, mp3.artist, mp3.title))

        # Actually de-duplicate the songs we've found.
        self._resolver.resolve_dupes([x[0] for x in dupes])
        return True

    def _cleanup(self):
        self._resolver.commit()


def run_deduper(db_url, solr_url, dry_run, filter_artist, filter_title):
    engine = init_db(db_url, solr_url)
    dup = DeDuper(engine, filter_artist, filter_title)
    dup.dedupe(dry_run)


def main():
    parser = optparse.OptionParser()
    parser.add_option('--db_url')
    parser.add_option('--solr_url', default='http://localhost:8080/solr')
    parser.add_option('--apply', action='store_true')
    parser.add_option('--filter_artist')
    parser.add_option('--filter_title')
    daemonize.add_standard_options(parser)
    utils.read_config_defaults(
        parser, os.getenv('DJRANDOM_CONF', '/etc/djrandom.conf'))
    opts, args = parser.parse_args()
    if not opts.db_url:
        parser.error('Must provide --db_url')
    if args:
        parser.error('Too many arguments')

    daemonize.daemonize(opts, run_deduper,
                        (opts.db_url, opts.solr_url, not opts.apply,
                         opts.filter_artist, opts.filter_title))


if __name__ == '__main__':
    main()
