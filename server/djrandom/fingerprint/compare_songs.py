import eyeD3
from djrandom.model.mp3 import MP3


# Monkey-patch eyeD3 so that it does not look at file extensions to
# figure out if something is an MP3 or not.
eyeD3.tag.isMp3File = lambda x: True


def _compare_score(a, b):
    a_bitrate, a_duration, a_nmeta = a[0]
    b_bitrate, b_duration, b_nmeta = b[0]
    res = cmp(a_bitrate, b_bitrate)
    if res == 0:
        res = cmp(a_duration, b_duration)
        if res == 0:
            res = cmp(a_nmeta, b_nmeta)
    return res


def get_song_score(mp3):
    try:
        af = eyeD3.Mp3AudioFile(mp3.path)
    except:
        return (0, 0, 0)

    # Get encoding parameters.
    bitrate = af.getBitRate()[1]
    duration = 30 * (int(af.getPlayTime()) / 30) # round to 30 secs

    # Count metadata tags.
    try:
        tag = af.getTag()
        has_album = not (not tag.getAlbum())
        has_artist = not (not tag.getArtist())
        has_title = not (not tag.getTitle())
        has_genre = not (not tag.getGenre())
        has_year = not (not tag.getYear())
        has_tracknum = (tag.getTrackNum()[0] is not None)
        has_images = not (not tag.getImages())
        num_meta = (4 * int(has_images) 
                    + 2 * sum(map(int, (has_album, has_artist, has_title)))
                    + sum(map(int, (has_genre, has_year, has_tracknum))))
    except:
        num_meta = 0

    return (bitrate, duration, num_meta)


def sort_songs(hashes=None, mp3s=None):
    assert hashes or mp3s
    if mp3s is None:
        mp3s = MP3.query.filter(MP3.sha1.in_(hashes))
    return sorted(((get_song_score(x), x.sha1) for x in mp3s),
                  cmp=_compare_score, reverse=True)

