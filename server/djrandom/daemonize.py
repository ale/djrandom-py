"""gevent-compatible daemonization."""

import logging
import logging.handlers
import os
import platform
import pwd
import sys
import syslog
import signal


log = logging.getLogger(__name__)
program_name = os.path.basename(sys.argv[0])


class PIDFile(object):

    def __init__(self, path):
        self.path = path

    def acquire(self):
        if os.path.exists(self.path):
            cur_pid = -1
            try:
                cur_pid = int(open(self.path).read().strip())
            except:
                sys.stderr.write(
                    'Warning: corrupted pidfile, overwriting...\n')
            if cur_pid > 0:
                try:
                    os.kill(cur_pid, 0)
                    raise RuntimeError(
                        'another instance is already running, pid=%d' % cur_pid)
                except OSError:
                    sys.stderr.write(
                        'Warning: stale lock found (pid=%d)\n' % cur_pid)
        fd = open(self.path, 'w', 0644)
        fd.write('%d\n' % os.getpid())
        fd.close()

    def release(self):
        try:
            os.unlink(self.path)
        except:
            pass


def setup_logging(foreground=False, debug=False, 
                  facility=syslog.LOG_DAEMON):
    logger = logging.getLogger()
    if foreground:
        logging.basicConfig()
    else:
        if platform.system() == 'Darwin':
            dev_log = '/var/run/syslog'
        else:
            dev_log = '/dev/log'
        handler = logging.handlers.SysLogHandler(address=dev_log,
                                                 facility=facility)
        handler.setFormatter(logging.Formatter(
                program_name + '[%(process)d]: (%(module)s) %(levelname)s: %(message)s'))
        logger.addHandler(handler)
    if debug:
        logger.setLevel(logging.DEBUG)
    else:
        logger.setLevel(logging.INFO)


def daemonize(opts, main_function, args, support_gevent=False):
    """Our own daemonizing function. 

    It will daemonize (or not) 'main_function' according to the
    command-line parameters specified in 'opts'.  

    Supported options:

      - opts.pidfile: where to write the PID file
      - opts.debug: set logging level to DEBUG
      - opts.foreground: do not detach from running terminal
      - opts.username: setuid() to this user

    We don't want to close the existing file descriptors, as gevent
    and logging need some of them.
    """
    if not opts.foreground:
        if os.fork():
            return
        if support_gevent:
            import gevent
            gevent.reinit()

    pidfile = PIDFile(opts.pidfile)
    
    try:
        pidfile.acquire()
    except Exception, e:
        sys.stderr.write('Error: could not write pidfile: %s\n' % str(e))
        sys.exit(1)

    if opts.username:
        try:
            user_pw = pwd.getpwnam(opts.username)
        except KeyError:
            sys.stderr.write(
                'Error: could not find user "%s"\n' % opts.username)
            sys.exit(1)
        os.chown(opts.pidfile, user_pw.pw_uid, user_pw.pw_gid)
        os.setuid(user_pw.pw_uid)

    if support_gevent:
        import gevent
        def _sighandler(signum):
            log.info('terminated by signal %d' % signum)
            pidfile.release()
            sys.exit(0)
        gevent.signal(signal.SIGTERM, _sighandler, 15)
        gevent.signal(signal.SIGINT, _sighandler, 3)
    else:
        def _sighandler(signum, frame):
            log.info('terminated by signal %d' % signum)
            pidfile.release()
            sys.exit(0)
        signal.signal(signal.SIGTERM, _sighandler)
        signal.signal(signal.SIGINT, _sighandler)            

    if not opts.foreground:
        null_fd = open(os.devnull, 'r+')
        for stream in (sys.stdin, sys.stdout, sys.stderr):
            os.dup2(null_fd.fileno(), stream.fileno())
        os.setsid()

    setup_logging(opts.foreground, opts.debug)

    try:
        main_function(*args)
    finally:
        pidfile.release()


def add_standard_options(parser):
    """Add some standard options to an OptionParser instance.

    The options are those mentioned in the 'daemonize' docstring.
    """
    parser.add_option('--debug', dest='debug', action='store_true',
                      help='Set log level to DEBUG')
    parser.add_option('--foreground', dest='foreground', action='store_true',
                      help='Do not fork into the background')
    parser.add_option('--pidfile', dest='pidfile', metavar='FILE',
                      default='/var/run/%s.pid' % program_name,
                      help='Location of the pid file')
    parser.add_option('--user', dest='username', metavar='NAME',
                      help='Optionally run as this user')
