import os
import optparse
import logging
import sys
import time
from djrandom import daemonize
from djrandom import utils
from djrandom.database import Session, init_db, indexer
from djrandom.model.mp3 import MP3

log = logging.getLogger(__name__)


def fix_solr():
    print 'rebuilding the SOLR index...'
    for count, mp3 in enumerate(MP3.query):
        indexer.add_mp3(mp3)
        if count % 100 == 0:
            sys.stdout.write('%d  \r' % count)
            sys.stdout.flush()
    print '\ncommit...'
    indexer.commit()
    print 'done.'


def run_fixer(db_url, solr_url):
    init_db(db_url, solr_url)
    fix_solr()


def main():
    parser = optparse.OptionParser()
    parser.add_option('--solr_url', default='http://localhost:8080/solr')
    parser.add_option('--db_url')
    daemonize.add_standard_options(parser)
    utils.read_config_defaults(
        parser, os.getenv('DJRANDOM_CONF', '/etc/djrandom.conf'))
    opts, args = parser.parse_args()
    if not opts.db_url:
        parser.error('Must provide --db_url')
    if args:
        parser.error('Too many arguments')

    daemonize.daemonize(opts, run_fixer,
                        (opts.db_url, opts.solr_url))


if __name__ == '__main__':
    main()
