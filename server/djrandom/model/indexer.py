import solr


class Indexer(object):
    """A very simple wrapper for Solr that supports lazy inizialization."""

    def __init__(self, solr_url=None):
        self._solr = None
        self._solr_url = solr_url

    def set_url(self, solr_url):
        self._solr_url = solr_url

    def _get_solr(self):
        if not self._solr:
            self._solr = solr.Solr(self._solr_url, timeout=30)
        return self._solr

    def add_mp3(self, mp3):
        # Almost equal to mp3.to_dict() but not exactly (SOLR calls 'id'
        # what the database calls 'sha1').
        if mp3.state == mp3.READY:
            self._get_solr().add({
                    'id': mp3.sha1,
                    'artist': mp3.artist,
                    'album': mp3.album,
                    'title': mp3.title,
                    'genre': mp3.genre})
        else:
            self._get_solr().delete(mp3.sha1)

    def del_mp3(self, mp3):
        self._get_solr().delete(mp3.sha1)

    def delete_all(self):
        self._get_solr().delete_query('id:*')

    def commit(self):
        self._get_solr().commit()
