import os
import base64
import crypt
from sqlalchemy import *
from datetime import datetime
from djrandom import utils
from djrandom.database import Base

ACTIVATION_TEMPLATE ='''
Hello %(email)s,

%(sent_by)s has invited you to %(site_name)s.

Click the following link to activate your account:

  %(activation_link)s



--
djrandom invitebot
http://%(site_name)s/
'''


def _salt():
    return '$6$' + base64.b64encode(os.urandom(12))


class APIKey(Base):

    __tablename__ = 'api_keys'

    key = Column(String(40), primary_key=True)
    userid = Column(Integer(), index=True)

    def __init__(self, userid):
        self.key = utils.random_token()
        self.userid = userid


class User(Base):
    """A user account."""

    __tablename__ = 'users'

    id = Column(Integer(), primary_key=True)
    email = Column(String(128), index=True, unique=True)
    password = Column(String(128))
    created_at = Column(DateTime())
    invited_by = Column(Integer())
    invites_left = Column(Integer())
    active = Column(Boolean())
    activation_token = Column(String(40))

    def __init__(self, email, password, invited_by=None):
        self.email = email
        self.set_password(password)
        self.active = False
        self.activation_token = utils.random_token()
        self.created_at = datetime.now()
        self.invited_by = invited_by
        self.invites_left = 3

    def set_password(self, password):
        self.password = crypt.crypt(password, _salt())

    def check_password(self, password):
        return (crypt.crypt(password, self.password) == self.password)

    def create_api_key(self):
        return APIKey(self.id)

    def get_activation_email(self, sent_by, site_name):
        activation_link = 'http://%s/user/activate/%s?e=%s' % (
            site_name, self.activation_token, self.email)
        return ACTIVATION_TEMPLATE % dict(
            site_name=site_name,
            email=self.email,
            sent_by=sent_by,
            activation_link=activation_link)

    @classmethod
    def find_with_api_key(cls, key):
        key_obj = APIKey.query.get(key)
        if key_obj:
            return cls.query.get(key_obj.userid)

