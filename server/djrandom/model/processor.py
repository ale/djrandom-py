import logging
import time
import traceback
from djrandom.database import Session

log = logging.getLogger(__name__)

COMMIT_EVERY = 50


class Processor(object):
    """Base class to perform operations on database objects.

    Used as the basis of most cron-based jobs, it provides enough
    abstraction to support a conversion to an event-based model if
    necessary.

    Subclasses should define at least the 'query' and 'process'
    methods.
    """

    def __init__(self, dry_run=False):
        self._dry_run = dry_run

    def full_scan(self):
        n = 0
        items = self.query()
        for item in items:
            try:
                self.process(item)
                Session.add(item)
            except Exception, e:
                log.error(
                    'Unexpected exception processing item %s:\n %s' % (
                        item, traceback.format_exc()))
            n += 1
            if (n % COMMIT_EVERY == 0) and not self._dry_run:
                Session.commit()
        if n > 0 and not self._dry_run:
            Session.commit()
            self.commit()
        return n

    def commit(self):
        """Optional commit hook for subclasses."""
        pass

    def query(self):
        raise NotImplemented()

    def process(self, item):
        raise NotImplemented()

    def loop(self, run_once=False, period=300):
        while True:
            n = 0
            try:
                n = self.full_scan()
            except Exception, e:
                log.exception('Temporary error')
            Session.remove()
            if run_once:
                break
            if not n:
                time.sleep(period)
