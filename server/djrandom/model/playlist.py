import os
import hashlib
from sqlalchemy import *
from djrandom import utils
from djrandom.database import Base


class Playlist(Base):

    __tablename__ = 'playlists'

    uuid = Column(String(40), primary_key=True)
    title = Column(Unicode(128), index=True)
    userid = Column(String(40), index=True)
    modified_at = Column(DateTime())
    play_count = Column(Integer(), default=0)
    contents = Column(Text())
    streaming = Column(Boolean(), default=False)

    def __init__(self, **kw):
        if kw and not kw.get('uuid'):
            kw['uuid'] = utils.random_token()
        for k, v in kw.items():
            setattr(self, k, v)

    def to_dict(self):
        return {'uuid': self.uuid,
                'title': self.title,
                'songs': self.contents.split(',')}

    def stream_url(self):
        return 'http://djrandom.incal.net:8000/%s.mp3' % self.uuid

    @classmethod
    def get_all_by_user(cls, userid, public=False):
        where = (cls.userid == userid)
        if public:
            where = where & (cls.title != None)
        return cls.query.filter(where).order_by(asc(cls.modified_at))

    @classmethod
    def get_by_title(cls, userid, title):
        return cls.query.filter_by(userid=userid, title=title).first()
