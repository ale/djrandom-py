import os
import optparse
import logging
import subprocess
import time
import tempfile
from djrandom import daemonize
from djrandom import utils
from djrandom.database import init_db, Session
from djrandom.model.mp3 import MP3
from djrandom.model.playlist import Playlist

log = logging.getLogger(__name__)

STREAM_TEMPLATE = '''
<ezstream>
  <url>%(url)s</url>
  <sourcepassword>%(password)s</sourcepassword>
  <format>MP3</format>
  <filename>plist.m3u</filename>
  <playlist_program>0</playlist_program>
  <svrinfoname>DJRANDOM</svrinfoname>
  <svrinfopublic>0</svrinfopublic>

  <reencode>
    <enable>1</enable>
    <encdec>
      <format>MP3</format>
      <match>.mp3</match>
      <decode>madplay -b 16 -R 44100 -S -o raw:- "@T@"</decode>
      <encode>lame --preset cbr 64 -r -s 44.1 --bitwidth 16 - -</encode> 
    </encdec>
  </reencode>
</ezstream>
'''


class Stream(object):

    def __init__(self, playlist, server_url, password):
        self.playlist_uuid = playlist.uuid
        self.songs = [MP3.query.get(sha1)
                      for sha1 in playlist.contents.split(',')]
        self.stream_url = '%s/%s' % (server_url.rstrip('/'), playlist.uuid)
        self.source_password = password

        self.dir = tempfile.mkdtemp()
        self.xml_path = os.path.join(self.dir, 'stream.xml')
        self.proc = None
        log.debug('created new temp dir %s' % self.dir)

    def _write_stream_xml(self):
        with open(self.xml_path, 'w', 0600) as fd:
            fd.write(STREAM_TEMPLATE % dict(
                    url=self.stream_url,
                    password=self.source_password))

    def _write_plist(self):
        with open(os.path.join(self.dir, 'plist.m3u'), 'w', 0600) as fd:
            for mp3 in self.songs:
                local_path = os.path.join(self.dir, '%s.mp3' % mp3.sha1)
                os.symlink(mp3.path, local_path)
                fd.write('%s\n' % local_path)

    def stop(self):
        if self.proc:
            log.debug('stopping ezstream process %d' % self.proc.pid)
            self.proc.kill(15)
        log.debug('cleaning up temp dir %s' % self.dir)
        subprocess.call(['rm', '-fr', self.dir])

    def start(self):
        self._write_plist()
        self._write_stream_xml()
        log.debug('starting ezstream -c %s' % self.xml_path)
        self.proc = subprocess.Popen(['ezstream', '-c', self.xml_path],
                                     cwd=self.dir)

    def is_running(self):
        return (self.proc and self.proc.poll() is None)


class StreamRunner(object):

    def __init__(self, server_url, password):
        self.server_url = server_url
        self.password = password
        self.active_streams = {}

    def close(self):
        for stream in self.active_streams.itervalues():
            stream.stop()

    def scan(self):
        active_uuids = set(self.active_streams.keys())
        for pl in Playlist.query.filter_by(streaming=True):
            if pl.uuid in active_uuids:
                active_uuids.remove(pl.uuid)
            else:
                log.info('starting stream %s' % pl.uuid)
                self.active_streams[pl.uuid] = Stream(
                    pl, self.server_url, self.password)
        # Remove streams that aren't active anymore.
        for uuid in active_uuids:
            log.info('removing stream %s: no longer active' % uuid)
            self.active_streams.pop(uuid).stop()

    def health_check(self):
        # Ensure that all the streams are running.
        for uuid, stream in self.active_streams.iteritems():
            if not stream.is_running():
                log.debug('%s is not running!' % uuid)
                stream.start()

    def run(self, run_once):
        while True:
            self.scan()
            self.health_check()
            if run_once:
                break
            time.sleep(30)


def run_streamer(db_url, icecast_url, icecast_password, run_once):
    init_db(db_url)
    st = StreamRunner(icecast_url, icecast_password)
    st.run(run_once)


def main():
    parser = optparse.OptionParser()
    parser.add_option('--once', action='store_true')
    parser.add_option('--icecast_url', default='http://localhost:8000')
    parser.add_option('--icecast_password')
    parser.add_option('--db_url')
    daemonize.add_standard_options(parser)
    utils.read_config_defaults(
        parser, os.getenv('DJRANDOM_CONF', '/etc/djrandom.conf'))
    opts, args = parser.parse_args()
    if not opts.db_url:
        parser.error('Must provide --db_url')
    if not opts.icecast_password:
        parser.error('Must provide --icecast_password')
    if args:
        parser.error('Too many arguments')

    if opts.once:
        opts.foreground = True

    daemonize.daemonize(opts, run_streamer,
                        (opts.db_url, opts.icecast_url, 
                         opts.icecast_password, opts.once))


if __name__ == '__main__':
    main()
