from sqlalchemy import create_engine
from sqlalchemy.interfaces import PoolListener
from sqlalchemy.orm import scoped_session, sessionmaker
from sqlalchemy.ext.declarative import declarative_base
from djrandom.model.indexer import Indexer


Session = scoped_session(sessionmaker(autocommit=False,
                                      autoflush=False))
Base = declarative_base()
Base.query = Session.query_property()

indexer = Indexer()


# We are storing paths as binary blobs, without forcing a charset
# encoding. SQLAlchemy/MySQL needs this class to safely do so.
class SetTextFactory(PoolListener):
    def connect(self, dbapi_con, con_record):
        dbapi_con.text_factory = str


def init_db(uri, solr_url=None):
    # Import all ORM modules here, so that 'create_all' can find them.
    from djrandom.model import mp3, playlist

    if uri.startswith('mysql://'):
        engine = create_engine(uri, listeners=[SetTextFactory()],
                               pool_recycle=1800)
    else:
        engine = create_engine(uri, pool_recycle=1800)

    Session.configure(bind=engine)
    Base.metadata.create_all(engine)

    # You can omit 'solr_url' if the program uses the db read-only.
    if solr_url:
        indexer.set_url(solr_url)

    return engine

