import logging
import os
import optparse
import shutil
from datetime import datetime
from flask import Flask, request, abort, jsonify
from djrandom import daemonize
from djrandom import utils
from djrandom.model.mp3 import MP3
from djrandom.database import Session, init_db


log = logging.getLogger(__name__)
app = Flask(__name__)
storage_root = None


class Error(Exception):
    pass


@app.teardown_request
def shutdown_dbsession(exception=None):
    Session.remove()


@app.route('/check/<sha1>')
def check(sha1):
    mp3 = MP3.query.get(sha1)
    return jsonify(status=(mp3 is not None))


def _upload_mp3(incoming_fd, sha1):
    mp3 = MP3(path=utils.generate_path(storage_root, sha1),
              state=MP3.INCOMING,
              sha1=sha1,
              uploaded_at=datetime.now())
    with open(mp3.path, 'w') as fd:
        shutil.copyfileobj(incoming_fd, fd)
    if utils.sha1_of_file(mp3.path) != sha1:
        log.error('sha1 mismatch')
        return False
    mp3.size = os.path.getsize(mp3.path)
    Session.add(mp3)
    Session.commit()
    log.info('successfully stored %s' % (sha1,))
    return True


@app.route('/upload/<sha1>', methods=['POST'])
def upload(sha1):
    if request.content_type != 'audio/mpeg':
        log.error('attempted upload with wrong content-type (%s)' % (
                request.content_type,))
        abort(400)
    if not _upload_mp3(request.stream, sha1):
        abort(400)
    return jsonify(status=True)


def run_receiver(port, storage_dir, db_url):
    global storage_root
    storage_root = storage_dir
    init_db(db_url)

    from gevent.wsgi import WSGIServer
    http_server = WSGIServer(('0.0.0.0', port), app)
    http_server.serve_forever()


def main():
    parser = optparse.OptionParser()
    parser.add_option('--port', type='int', default=3002)
    parser.add_option('--storage_dir')
    parser.add_option('--db_url')
    daemonize.add_standard_options(parser)
    utils.read_config_defaults(
        parser, os.getenv('DJRANDOM_CONF', '/etc/djrandom.conf'))
    opts, args = parser.parse_args()
    if not opts.storage_dir:
        parser.error('Must provide --storage_dir')
    if not opts.db_url:
        parser.error('Must provide --db_url')
    if args:
        parser.error('Too many arguments')

    daemonize.daemonize(opts, run_receiver,
                        (opts.port, opts.storage_dir, opts.db_url))


if __name__ == '__main__':
    main()
