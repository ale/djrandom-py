import eyeD3
import re


def normalize_string(s):
    return re.sub(r'\W+', ' ', s, re.UNICODE).lower()


def analyze_mp3(path):
    tag = eyeD3.Tag()
    tag.link(path)
    try:
        genre = tag.getGenre().getName()
    except:
        genre = '' 
    return {'artist': normalize_string(tag.getArtist()),
            'album': normalize_string(tag.getAlbum()),
            'title': normalize_string(tag.getTitle()),
            'genre': unicode(genre).lower()}
