import smtplib
from email.mime.text import MIMEText


class Mailer(object):

    def __init__(self, sender):
        self.sender = sender

    def send(self, to, subject, message):
        msg = MIMEText(message)
        msg['Subject'] = '[DJ:Random] %s' % (subject,)
        msg['From'] = self.sender
        msg['To'] = to

        conn = smtplib.SMTP('localhost')
        conn.sendmail(self.sender, [to], msg.as_string())
        conn.quit()

