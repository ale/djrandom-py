#!/usr/bin/python
#
# WSGI application file for Apache mod_wsgi.
#


import logging
import os
from djrandom import utils
from djrandom.frontend import wsgi_app


class _Config(object):

    def __init__(self):
        self.app_config = '/etc/djrandom-app.conf'
        self.solr_url = 'http://localhost:8080/solr'
        self.markov_data = '/var/lib/djrandom/djrandom-markov.dat'
        self.album_art_dir = '/var/cache/djrandom/album-images'
        self.email_sender = 'djrandom'
        self.memcached_url = None
        self.db_url = None
        self.lastfm_api_key = None
        self.profile = False

    def set_default(self, key, value):
        setattr(self, key, value)


# Set up logging.
# Messages will end up in the main Apache error.log file.
logging.basicConfig(level=logging.INFO)

# Read configuration from file.
# Override the file location by setting DJRANDOM_CONF in the
# mod_wsgi environment.
_conf = _Config()
utils.read_config_defaults(
    _conf, os.getenv('DJRANDOM_CONF', '/etc/djrandom.conf'))
if not _conf.db_url:
    raise Exception('Must set db_url in DJRANDOM_CONF')

# Create the WSGI application for mod_wsgi.
application = wsgi_app.create_app(_conf)
