from flask import Flask, request, Response, session, g
from djrandom.database import Session
from djrandom.model.user import User
from functools import wraps

# The main Flask WSGI application.
app = Flask(__name__)

# Hopefully this will be changed by the app-specific configuration.
app.secret_key = 'J@9als[13- "!>0@!!zWz}='

# A bunch of (thread-safe) global instances.
svcs = dict(searcher=None,
            album_images=None,
            mailer=None)


@app.teardown_request
def shutdown_dbsession(exception=None):
    Session.remove()


def require_auth(f):
    @wraps(f)
    def check_auth_wrapper(*args, **kwargs):
        if app.config.get('TESTING'):
            g.userid = 'testuser'
        elif 'userid' in session:
            g.userid = session['userid']
        elif 'X-Key' in request.headers:
            user = User.find_with_api_key(
                request.headers['X-Key'])
            if user:
                g.userid = user.id
            else:
                return Response('API Key Error', 403)
        else:
            return Response('Login required', 302,
                            {'Location': '/login'})
        return f(*args, **kwargs)
    return check_auth_wrapper



# Import all the views.
import djrandom.frontend.views
import djrandom.frontend.api_views
import djrandom.frontend.user_views
