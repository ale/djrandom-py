from gevent import monkey
monkey.patch_all()

import sys
import os
import optparse
import logging
from djrandom import daemonize
from djrandom import utils
from djrandom.frontend import wsgi_app
from gevent.wsgi import WSGIServer

log = logging.getLogger(__name__)


def run_frontend(opts):
    app = wsgi_app.create_app(opts, True)
    http_server = WSGIServer((opts.bind_address, opts.port), app)
    http_server.serve_forever()


def main():
    parser = optparse.OptionParser()
    parser.add_option('--solr_url', default='http://localhost:8080/solr')
    parser.add_option('--bind_address', default='127.0.0.1')
    parser.add_option('--port', type='int', default=3003)
    parser.add_option('--db_url')
    parser.add_option('--lastfm_api_key')
    parser.add_option('--email_sender', default='djrandom')
    parser.add_option('--album_art_dir',
                      default='/var/cache/djrandom/album-images')
    parser.add_option('--markov_data',
                      default='/var/lib/djrandom/djrandom-markov.dat')
    parser.add_option('--memcached_url')
    parser.add_option('--profile', action='store_true')
    parser.add_option('--app_config')
    daemonize.add_standard_options(parser)
    utils.read_config_defaults(
        parser, os.getenv('DJRANDOM_CONF', '/etc/djrandom.conf'))
    opts, args = parser.parse_args()
    if not opts.db_url:
        parser.error('Must provide --db_url')
    if not opts.app_config:
        parser.error('Must provide --app_config')
    if args:
        parser.error('Too many arguments')

    daemonize.daemonize(opts, run_frontend, (opts,),
                        support_gevent=True)


if __name__ == '__main__':
    main()
