import os
import logging
import pygooglechart
from datetime import datetime
from flask import Flask, request, Response, abort, jsonify, render_template, g
from djrandom import utils
from djrandom.database import Session
from djrandom.model.mp3 import MP3, PlayLog
from djrandom.model.playlist import Playlist
from djrandom.model.user import User
from djrandom.frontend import app, require_auth, svcs
from sqlalchemy import func

log = logging.getLogger(__name__)
use_xsendfile = 1


@app.route('/autocomplete')
@require_auth
def autocomplete_search():
    results = []
    term = request.args.get('q')
    if not term:
        abort(400)
    if len(term) > 1:
        n_results = int(request.args.get('limit', 50))
        cache_key = 'autocomplete|%s|%d' % (term, n_results)
        results = svcs['cache'].get(cache_key)
        if not results:
            artists = Session.query(
                MP3.artist, func.count('*').label('count')).filter(
                MP3.artist.like('%s%%' % term)).group_by(
                    MP3.artist).order_by('count desc').limit(n_results)

            results = [x[0] for x in artists]
            svcs['cache'].set(cache_key, results, timeout=86400)
    return '\n'.join(results)


@app.route('/fragment/songs', methods=['POST'])
@require_auth
def songs_fragment():
    hashes = request.form.get('h', '')
    if hashes:
        mp3s = MP3.get_many(hashes.split(','))
    else:
        mp3s = []
    return render_template('songs_fragment.html', songs=mp3s)


def _makegraph(values):
    max_y = max(values) * 1.1
    chart = pygooglechart.SparkLineChart(220, 80, y_range=[0, max_y])
    chart.add_data(values)
    chart.set_colours(['0000FF'])
    return chart.get_url()


@app.route('/about')
@require_auth
def about():
    cache_key = 'about'
    data = svcs['cache'].get(cache_key)
    if not data:
        data = {
            'num_songs': MP3.query.filter_by(state=MP3.READY).count(),
            'dup_songs': MP3.query.filter_by(state=MP3.DUPLICATE).count(),
            'used_gb': int(Session.query(func.sum(
                        MP3.size)).first()[0] / (2 << 29)),
            'num_users': User.query.filter_by(active=True).count(),
            'incoming_graph': _makegraph(MP3.uploads_by_day(30)),
            'play_graph': _makegraph(PlayLog.plays_by_day(30)),
            }
        svcs['cache'].set(cache_key, data, timeout=9600)

    return render_template('about.html', data=data, config=app.config)


@app.route('/')
@require_auth
def homepage():
    user = User.query.get(g.userid)
    return render_template('index.html', user=user, config=app.config)


@app.route('/ext')
@require_auth
def redirect_to_external_url():
    url = request.args.get('url')
    if not url:
        abort(400)
    return render_template('redirect.html', url=url)


def fileiter(path, pos, end):
    with open(path, 'r') as fd:
        fd.seek(pos)
        while True:
            n = min(65536, end - pos)
            chunk = fd.read(n)
            if not chunk:
                break
            yield chunk
            pos += len(chunk)


def download_song_python(mp3):
    # Parse 'Range' HTTP header. This is a very dumb parser, it will
    # only support a single range spec.
    start = 0
    end = os.path.getsize(mp3.path)
    partial = False
    if 'Range' in request.headers:
        partial = True
        hrange = request.headers['Range']
        if hrange.startswith('bytes='):
            hrange = hrange[6:]
        if '-' in hrange:
            sstart, send = hrange.split('-')
            if sstart:
                start = int(sstart)
            if send:
                end = int(send)
        else:
            end = int(hrange)

    # Create response with content iterator.
    response = Response(fileiter(mp3.path, start, end),
                        status=(partial and 206 or 200),
                        direct_passthrough=True)
    response.headers['Accept-Ranges'] = 'bytes'
    if partial:
        response.headers['Content-Range'] = 'bytes %d-%d/%d' % (
            start, end - 1, os.path.getsize(mp3.path))
    response.content_type = 'audio/mpeg'
    response.content_length = (end - start)
    return response


def download_song_xsendfile(mp3):
    return Response('', status=200,
                    content_type = 'audio/mpeg',
                    headers={'X-SENDFILE': mp3.path})


@app.route('/dl/<sha1>')
@require_auth
def download_song(sha1):
    mp3 = MP3.query.get(sha1)
    if not mp3:
        abort(404)

    # This is a good time to check round-trip inconsistencies: if the
    # user is requesting a duplicate file, it means our SOLR index got
    # out of sync, but we can still give him the file he wants.
    if mp3.state == MP3.DUPLICATE:
        dup_mp3 = MP3.query.get(mp3.duplicate_of)
        if not dup_mp3:
            log.error(
                'SEVERE: duplicate_of %s points to non-existing mp3 %s' % (
                    mp3.sha1, mp3.duplicate_of))
            abort(404)
        log.error(
            'inconsistency: user got duplicate song: %s (instead of %s)' % (
                mp3.sha1, mp3.duplicate_of))
        mp3 = dup_mp3
    
    if mp3.state != MP3.READY:
        log.error('inconsistency: user got non-ready song: %s' % mp3.sha1)
        abort(404)

    if not os.path.exists(mp3.path):
        log.error('SEVERE: file has disappeared for song: %s' % mp3.sha1)
        abort(404)

    if use_xsendfile:
        return download_song_xsendfile(mp3)
    else:
        return download_song_python(mp3)

