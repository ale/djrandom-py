import logging
import solr
from collections import defaultdict

log = logging.getLogger(__name__)


class Searcher(object):

    def __init__(self, solr_url):
        self.solr = solr.Solr(solr_url, timeout=30)

    def search(self, qstr, n=10, partial=False):
        try:
            results = self.solr.select(q=qstr, sort='score desc',
                                       fields='score,id', start=0, rows=n)
        except solr.SolrException, e:
            log.error('SOLR exception (query="%s"): %s' % (
                qstr, e))
            return
        for doc in results.results:
            yield doc['score'], doc['id']

    def more_like_these(self, hashes, n=5):
        """Return a set of hashes that are related to the ones provided."""
        query_str = ' OR '.join('(id:%s)' % x for x in hashes if x)
        try:
            results = self.solr.select(
                q=query_str, fields='score,id', rows=n,
                mlt='true', mlt_fl='text', mlt_mindf=1, mlt_mintf=1)
        except solr.SolrException, e:
            log.error('SOLR exception (query="%s"): %s' % (
                query_str, e))
            return []

        # Parse and uniq-ify the results.
        hashes_set = set(hashes)
        more = defaultdict(lambda: 0)
        for doclist in results.moreLikeThis.itervalues():
            for doc in doclist:
                if doc['id'] not in hashes_set:
                    more[doc['id']] += doc['score']
        results = more.keys()
        results.sort(reverse=True, key=lambda x: more[x])
        return results
