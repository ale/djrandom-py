import logging
from datetime import datetime
from flask import Flask, request, Response, redirect, abort, session, \
    render_template, flash, g
from flaskext.wtf import *
from djrandom import utils
from djrandom.database import Session
from djrandom.model.user import User, APIKey
from djrandom.model.playlist import Playlist
from djrandom.frontend import app, require_auth, svcs

log = logging.getLogger(__name__)


class UserDetailsForm(Form):
    email = TextField('Email', [
            validators.Required(), 
            validators.Email()])
    password = PasswordField('Password')
    password_confirm = PasswordField('Confirm Password')

    def validate_password(self, field):
        if field.data:
            if len(field.data) < 6:
                raise ValidationError('Password is too short (min. 6 chars)')
            if self.password_confirm.data != field.data:
                raise ValidationError('Passwords are different')


def is_new_user(form, field):
    if User.query.filter_by(email=str(field.data)).first():
        raise ValidationError('This user has already been invited')


class InviteForm(Form):
    email = TextField('Email to invite', [validators.Email(), is_new_user])


class LoginForm(Form):
    email = TextField('Email', [validators.Required()])
    password = PasswordField('Password', [validators.Required()])


@app.route('/login', methods=['GET', 'POST'])
def login():
    form = LoginForm()
    login_failed = False
    if form.validate_on_submit():
        email = form.email.data
        user = User.query.filter_by(email=email).first()
        if user and user.check_password(form.password.data):
            session['userid'] = user.id
            session.permanent = True
            return redirect('/')
        log.info('failed login for %s' % email)
        login_failed = True
    return render_template('login.html', form=form, login_failed=login_failed,
                           config=app.config)


@app.route('/logout')
def logout():
    session.pop('userid', None)
    return Response('Logged out.', 200)


@app.route('/user/details', methods=['GET', 'POST'])
@require_auth
def user_details():
    user = User.query.get(g.userid)
    form = UserDetailsForm(email=user.email)
    if form.validate_on_submit():
        user.email = form.email.data
        if form.password.data:
            user.set_password(form.password.data)
            flash('password changed')
        Session.add(user)
        Session.commit()
    api_keys = APIKey.query.filter_by(userid=user.id)
    return render_template('user_details.html', user=user, form=form,
                           api_keys=api_keys, config=app.config)


@app.route('/user/apikey/create', methods=['POST'])
@require_auth
def user_create_api_key():
    user = User.query.get(g.userid)
    key = user.create_api_key()
    Session.add(key)
    Session.commit()
    return redirect('/user/details')


@app.route('/user/apikey/revoke/<keyid>')
@require_auth
def user_revoke_api_key(keyid):
    key = APIKey.query.get(keyid)
    if key.userid != g.userid:
        abort(403)
    Session.delete(key)
    Session.commit()
    return redirect('/user/details')


@app.route('/user/invite', methods=['GET', 'POST'])
@require_auth
def user_send_invite():
    user = User.query.get(g.userid)
    if not user.invites_left:
        abort(403)

    form = InviteForm()
    if form.validate_on_submit():
        user = User.query.get(g.userid)
        username = user.email.split('@')[0]

        email = form.email.data
        new_user = User(email, 'x', invited_by=g.userid)
        svcs['mailer'].send(
            email, 'Invitation',
            new_user.get_activation_email(
                username,
                app.config['SERVER_NAME']))

        user.invites_left -= 1
        Session.add(user)
        Session.add(new_user)
        Session.commit()
        flash('invitation sent to %s' % email)
        return redirect('/user/details')

    return render_template('user_invite.html', form=form, config=app.config)


@app.route('/user/activate/<token>')
def user_activate(token):
    email = request.args.get('e')
    user = User.query.filter_by(activation_token=token).first()
    if not user or user.active or user.email != email:
        abort(404)
    session['userid'] = user.id
    user.active = True
    Session.add(user)
    Session.commit()
    log.info('activated user %s' % user.email)
    return redirect('/user/details')
