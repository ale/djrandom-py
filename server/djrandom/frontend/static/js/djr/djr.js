// djr.js

// Globals and namespace definition.


// Namespace.
djr = {};

// Global state (with Player instance).
djr.state = {
  userid: null,
  backend: null,
  player: null,
  loadingCount: 0
};

// Initialize the Player and add all our onclick handlers.
djr.init = function (userid) {
  djr.state.userid = userid;
  djr.state.backend = new djr.Backend();
  djr.state.player = new djr.Player(djr.state.backend, '#djr_player');

  // Set a callback on URL hash changes.
  $(window).bind('hashchange', djr.history.restore);
  djr.history.restore();

  // Set autocompletion and search handlers.
  $('#queryField').autocomplete('/autocomplete', {
    queryParamName: 'term',
    maxItemsToShow: 50,
    delay: 150,
    minChars: 2
  });

  $('#queryField').focus();
  $('#searchForm').submit(function() {
    var query_string = $('#queryField').val();
    djr.state.player.search(query_string);
    return false;
  });
  $('#searchWarnMsg').click(function() {
    $(this).hide();
  });

  // Add onclick hooks to the playlist controls.
  $('#skipbtn').click(function() {
    djr.player().skipSong();
  });
  $('#playlistClearBtn').click(function() {
    djr.player().clearPlaylist();
  });
  $('#playlistStreamBtn').click(function() {
    djr.player().streamCurrentPlaylist();
  });
  $('#playlistExtendBtn').click(function() {
    djr.player().extendCurrentPlaylist();
  });
  $('#playlistLast5').click(function() {
    djr.player().lastPlaylist(5);
  });
  $('#playlistLast25').click(function() {
    djr.player().lastPlaylist(25);
  });
  $('#playlistRandom5').click(function() {
    djr.player().randomPlaylist(5);
  });
  $('#playlistRandom25').click(function() {
    djr.player().randomPlaylist(25);
  });
  $('#playlistMost5').click(function() {
    djr.player().mostPlayedPlaylist(5);
  });
  $('#playlistMost25').click(function() {
    djr.player().mostPlayedPlaylist(25);
  });
  $('#playlistNever5').click(function() {
    djr.player().neverPlayedPlaylist(5);
  });
  $('#playlistNever25').click(function() {
    djr.player().neverPlayedPlaylist(25);
  });
  $('#playlistSave').click(function() {
    if ( $('#saveForm').is(':visible') == false ) {
      if (djr.playlist && djr.playlist.title) {
        $('#savetext').val(djr.playlist.title);
      }
      $('#saveForm').show('fast');
      $('#savetext').focus();
    } else {
      $('#savetext').val('');
      $('#saveForm').hide();
    }
  });
  $('#playlistSaveBtn').click(function() {
    var pltitle = $('#savetext').val();
    if (pltitle != '') {
      djr.player().savePlaylistWithTitle(pltitle);
      djr.updatePlaylists();
    }
    $('#saveForm').hide();
  });
  $('#wikibtn').click(function () {
    var stitle = $('#song_' + djr.player().cur_song + ' .artist').text();
    if ( $('#wikipedia').is(':visible') == false ) {
      if ( stitle != "" ) { 
        stitle = stitle.split(' ').join('+');
        $('#wikipedia').show('slow');
        //$('#wikipedia').attr("src", "/ext?url=" + escape("http://en.wikipedia.org/wiki/Special:Search?search=" + stitle + "&go=Go"));
        $('#wikipedia').attr("src", "http://en.m.wikipedia.org/w/index.php?search=" + stitle);
      }
    } else {
        $('#wikipedia').hide('slow')
    }
  });
  $('#lastfmbtn').click(function () {
    var stitle = $('#song_' + djr.player().cur_song + ' .title').text();
    var sartist = $('#song_' + djr.player().cur_song + ' .artist').text();
    if ( $('#lastfm').is(':visible') == false ) {
      if ( stitle != "" ) { 
        stitle = stitle.split(' ').join('+');
        sartist = sartist.split(' ').join('+');
        $('#lastfm').show('slow');
        //$('#lastfm').attr("src", "/ext?url=" + escape("http://www.lastfm.com/#&q=" + stitle + sartist));
        $('#lastfm').attr("src", "http://m.last.fm/search?q=" + stitle + sartist );
      }
    } else {
        $('#lastfm').hide('slow')
    }
  });
  $('#lyricsbtn').click(function () {
    var stitle = $('#song_' + djr.player().cur_song + ' .title').text();
    if ( $('#lyrics').is(':visible') == false ) {
      if ( stitle != "" ) { 
        stitle = stitle.split(' ').join('+');
        $('#lyrics').show('slow');
        //$('#lyrics').attr("src", "/ext?url=" + escape("http://www.lyrics.com/#&q=" + stitle));
        $('#lyrics').attr("src", "http://lyrics.wikia.com/index.php?search=" + stitle + "&fulltext=0" );
      }
    } else {
        $('#lyrics').hide('slow')
    }
  });
  $('#pllistbtn').click(function () {
    if ($('#pllist').is(':visible') == false) {
      djr.updatePlaylists();
      $('#pllist').show('slow');
    } else {
      $('#pllist').hide('slow')
    }
  });

  // Set the album art image to auto-fullscreen on load.
  $('#albumart_fs').load(function() {
    $(this).fullBg();
    $(this).show();
  });

  djr.debug('initialization done.');

  djr.updatePlaylists();
};

// Export the player for quick onclick access
djr.player = function() {
  return djr.state.player;
};

// Update the list of playlists.
djr.updatePlaylists = function() {
  djr.state.backend.getPlList(djr.state.userid, function(results) {
    if (results.length == 0) {
      djr.debug('No playlists found.');
      return;
    }
    var output = '<ul>';
    $.each(results, function(idx, x) {
      if (x.title) {
        output += "<li><a onclick='djr.player().loadPlaylist(\"" + x.uuid + "\")' >"  + x.title + "</a></li>";
      }
    });
    output += '</ul>';
    $('#pllist').html(output);
  });
};

// Show/hide the 'loader' animated GIF.
// Keep a counter so we can nest calls.
djr.loading = function(active) {
  var loader = $('#loaderImg');
  if (active) {
    djr.state.loadingCount += 1;
  } else {
    djr.state.loadingCount -= 1;
  }
  if (djr.state.loadingCount > 0) {
    loader.show();
  } else {
    loader.hide();
  }
};

// Debugging.
djr.debug = function(msg) {
  var n_logs = $('#debug p').length;
  if (n_logs > 7) {
    $('#debug p:first').remove();
  }
  $('#debug').append('<p>' + msg + '</p>');
};

// Report a player error.
djr.playerError = function(event) {
  console.log(event.jPlayer.error);
  switch(event.jPlayer.error.type) {
  case $.jPlayer.error.URL:
    djr.debug('Error downloading song, skipping...')
    if (djr.state.player) {
      djr.state.player.nextSong();
    }
    break;
  default:
    djr.debug('Error: unexpected error (' + event.jPlayer.error.message + ')');
    break;
  }
};

// Show a search-related warning message.
djr.showSearchWarning = function(msg) {
  $('#searchWarnMsg').text(msg).show('fast');
};

djr.clearSearchWarning = function() {
  $('#searchWarnMsg').hide();
};

// Generate a random UUID.
var CHARS = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'.split('');
djr.generateRandomId = function() {
  var uuid = [], chars = CHARS, i;
  var len = 40, radix = chars.length;
  for (i = 0; i < len; i++) uuid[i] = chars[0 | Math.random()*radix];
  return uuid.join('');
};

// Add the escapeHTML() function to the String base class.
String.prototype.escapeHTML = function () {
  return(
    this.replace(/&/g,'&amp;').
      replace(/>/g,'&gt;').
      replace(/</g,'&lt;').
      replace(/"/g,'&quot;')
  );
};

