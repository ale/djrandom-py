// history.js

// History and state

djr.history = {
  cur_hash: null
};

// Save current state to hashtag.
djr.history.save = function(pl) {
  var hash_tag = '#pl:' + pl;
  djr.history.cur_hash = hash_tag;
  window.location.hash = hash_tag;
};

// Restore current state from hashtag.
djr.history.restore = function() {
  var hash_tag = window.location.hash;
  if (!hash_tag || hash_tag == '#' || hash_tag == djr.history.cur_hash) {
    return;
  }
  djr.history.cur_hash = hash_tag;

  // Parse the hash tag.
  var hash_info = {};
  $.each(hash_tag.substr(1).split(','), function(idx, value) {
    var kv = value.split(':');
    hash_info[kv[0]] = unescape(kv.slice(1).join(':'));
  });

  // Load the new playlist if specified.
  if (hash_info.pl) {
    djr.state.player.loadPlaylist(hash_info.pl);
  }
};

djr.doSearch = function() {
  $('#loading').show();
  $.getJSON('/json/search', {'q': djr.getQuery()},
            function(data, status, jqxhr) {
                // Build an ordered array of unique hashes, which will be
              // our new playlist.
              var hashes = Array();
              $.each(data.results, function(idx, item) {
                if ($.inArray(item.sha1, hashes) < 0) {
                  hashes.push(item.sha1);
                }
              });

              // Clear the current playlist and save a new one.
              if (hashes.length > 0) {
                djr.state.player.loadSearchResults(hashes, false);
              }

              // Load the HTML rendering of the playlist.
              djr.loadPlaylistHtml();

              djr.history.save();
          });
};



// Puppa.
