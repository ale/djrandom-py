// backend.js

/**
 * Backend API.
 *
 * This object provides simple wrappers for the most common AJAX calls
 * to the backend.
 *
 * Note: methods that take a 'callback' parameter also accept a
 * context parameter 'ctx', in case your callbacks need to use 'this'
 * (also, I suck at Javascript OOP, so it's entirely possible there's
 * an easier way to do it)...
 *
 * @constructor
 * @this {Backend}
 */
djr.Backend = function() {
  // No state yet.
};

// Wrap an AJAX call with some UI feedback.
var wrap_with_loader = function(ajaxData) {
  djr.loading(true);
  var old_success_handler = ajaxData.success;
  ajaxData.success = function(data, status, jqxhr) {
    old_success_handler(data, status, jqxhr);
    djr.loading(false);
  };
  var old_error_handler = ajaxData.error;
  ajaxData.error = function(jqXHR, textStatus, errorThrown) {
    djr.loading(false);
  };
  return $.ajax(ajaxData);
};


/**
 * Search.
 *
 * It will call 'callback(results)' where results is an array of song
 * objects that will have the 'sha1' and 'score' attributes.
 *
 * @param {string} query The search query.
 * @param {function} callback Callback function.
 * @param {Object} ctx Callback context.
 */
djr.Backend.prototype.search = function(query, callback, ctx) {
  wrap_with_loader(
    {url: '/json/search',
     data: {'q': query},
     dataType: 'json',
     type: 'GET',
     context: ctx || this,
     success: function(data, status, jqxhr) {
       callback(data.results);
     }
    });
};

/**
 * Return results similar to the provided songs.
 *
 * @param {Array[string]} hashes SHA1 hashes of the input songs.
 * @param {function} callback Callback function.
 * @param {Object} ctx Callback context.
 */
djr.Backend.prototype.moreLikeThese = function(n, uuids, callback, ctx) {
  wrap_with_loader(
    {url: '/json/morelikethese',
     data: {'h': uuids.join(','), 'n': n},
     dataType: 'json',
     type: 'POST',
     context: ctx || this,
     success: function(data, status, jqxhr) {
       callback(data.results);
     }
    });
};

/**
 * Autocomplete.
 *
 * Autocompletion callback suitable for use with the jQuery UI
 * autocomplete plugin.
 *
 * @param {Object} request request object (contains term).
 * @param {function} callback Callback function.
 */
djr.Backend.prototype.autocomplete = function(request, callback) {
  $.ajax({url: '/autocomplete', 
          data: {'term': request.term},
          dataType: 'json',
          success: function(data) {
            callback(data.results);
          },
          error: function(xhr, status, errorThrown) {
            callback();
          }});  
};

/**
 * Get the playlist list for a specified user.
 *
 * It will invoke callback(songs) where 'playlist' is an array of uuid
 * song hashes.
 *
 * @param {string} userid User id.
 * @param {function} callback Callback function.
 * @param {Object} ctx Callback context.
 */
djr.Backend.prototype.getPlList = function(userid, callback, ctx) {
  $.ajax({url: '/json/playlist/list/' + userid,
          dataType: 'json',
          type: 'GET',
          context: ctx || this,
          success: function(data, status, jqxhr) {
            callback(data.results);
          }
         });
};

/**
 * Get the songs in a playlist.
 *
 * It will invoke callback(songs) where 'songs' is an array of SHA1
 * song hashes.
 *
 * @param {string} uuid Playlist identifier.
 * @param {function} callback Callback function.
 * @param {Object} ctx Callback context.
 */
djr.Backend.prototype.getPlaylist = function(uuid, callback, ctx) {
  $.ajax({url: '/json/playlist/get/' + uuid,
          dataType: 'json',
          type: 'GET',
          context: ctx || this,
          success: function(data, status, jqxhr) {
            callback(data);
          }
         });
};

/**
 * Update contents of a playlist.
 *
 * Will create the playlist if necessary. You're supposed to generate
 * the unique playlist identifier on the caller side.
 *
 * @param {string} uuid Playlist identifier.
 * @param {Array[string]} songs SHA1 hashes of the songs.
 */
djr.Backend.prototype.savePlaylist = function(uuid, title, songs) {
  $.ajax({url: '/json/playlist/save',
          data: {uuid: uuid,
                 title: title,
                 h: songs.join(',')},
          type: 'POST'
         });
};

/**
 * Enable/disable streaming for a playlist.
 *
 * Invokes callback() with a single parameter: an object that has a
 * boolean 'stream' attribute, and an optional 'url' attribute
 * containing the actual stream url (only present if streaming is
 * enabled).
 *
 * @param {string} uuid Playlist identifier.
 * @param {bool} streaming Enable streaming for this playlist.
 * @param {function} callback Callback function.
 * @param {Object} ctx Callback context.
 */
djr.Backend.prototype.streamPlaylist = function(uuid, streaming, callback, ctx) {
  $.ajax({url: '/json/playlist/stream',
          data: {uuid: uuid,
                 stream: streaming ? 'y' : 'n'},
          dataType: 'json',
          type: 'POST',
          context: ctx || this,
          success: function(data, status, jqxhr) {
            callback(data);
          }
         });
};

/**
 * Return song information.
 *
 * @param {string} song SHA1 of the song
 */
djr.Backend.prototype.getSongInfo = function(song, callback) {
  $.ajax({url: '/json/song/' + song,
          dataType: 'json',
          type: 'GET',
          success: function(data, status, jqxhr) {
            callback(data);
          }
         });
};

/**
 * Return the HTML fragment to render a list of songs.
 *
 * The callback function will be called with the resulting HTML string
 * as its first argument.
 *
 * @param {Array[string]} songs SHA1 hashes of the songs.
 * @param {function} callback Callback function.
 * @param {Object} ctx Callback context.
 */
djr.Backend.prototype.getHtmlForSongs = function(songs, callback, ctx) {
  wrap_with_loader(
    {url: '/fragment/songs',
     data: {'h': songs.join(',')},
     dataType: 'html',
     type: 'POST',
     context: ctx || this,
     success: callback
    });
};

/**
 * Report that a new song is playing.
 *
 * Together with the hash of the current song, you can also send a
 * short list of the most recent songs that have been played.
 *
 * @param {string} song SHA1 hash of the song that is about to start
 *    playing.
 * @param {Array[string]} old_songs SHA1 hashes of the previously 
 *    played songs, in order from the oldest to the newest.
 */
djr.Backend.prototype.nowPlaying = function(song, old_songs) {
  $.ajax({url: '/json/playing',
          data: {'cur': song,
                 'prev': old_songs.join(',')},
          type: 'POST'
          });
};

/**
 * Report that we skipped a song.
 *
 * @param {string} song SHA1 hash of the song that was skipped
 */
djr.Backend.prototype.skipSong = function(song) {
  $.ajax({url: '/json/skip',
          data: {'cur': song},
          type: 'POST'
          });
};

/**
  * Request N never played songs
  *
  * @param {integer} n Number of songs requested
  *
  */
djr.Backend.prototype.neverPlayedPlaylist = function(num, callback ,ctx) {
  wrap_with_loader(
    {url: '/json/never_played',
     data: {n: num },
     dataType: 'json',
     type: 'GET',
     context: ctx || this,
     success: function(data, status, jqxhr) {
       callback(data.results);
     }
    });
};

/**
  * Request N most played songs
  *
  * @param {integer} n Number of songs requested
  *
  */
djr.Backend.prototype.mostPlayedPlaylist = function(num, callback ,ctx) {
  wrap_with_loader(
    {url: '/json/most_played',
     data: {n: num },
     dataType: 'json',
     type: 'GET',
     context: ctx || this,
     success: function(data, status, jqxhr) {
       callback(data.results);
     }
    });
};

/**
  * Request N most played songs
  *
  * @param {integer} n Number of songs requested
  *
  */
djr.Backend.prototype.lastPlaylist = function(num, callback ,ctx) {
  wrap_with_loader(
    {url: '/json/last_uploaded',
     data: {n: num },
     dataType: 'json',
     type: 'GET',
     context: ctx || this,
     success: function(data, status, jqxhr) {
       callback(data.results);
     }
    });
};

/**
  * Return N pseudo-random songs based on the current playlist.
  *
  * @param {integer} n Number of songs requested
  * @param {Array[string]} uuids SHA1 hashes of the current playlist
  *
  */
djr.Backend.prototype.markovPlaylist = function(num, uuids, callback ,ctx) {
  wrap_with_loader(
    {url: '/json/markov',
     data: {'h': uuids.join(','),
            'n': num },
     dataType: 'json',
     type: 'POST',
     context: ctx || this,
     success: function(data, status, jqxhr) {
       callback(data.results);
     }
    });
};

/**
 * Return the Markovian probability vector for a playlist.
 */
djr.Backend.prototype.markovVector = function(uuids, callback ,ctx) {
  wrap_with_loader(
    {url: '/json/markov_vector',
     data: {'h': uuids.join(',')},
     dataType: 'json',
     type: 'GET',
     context: ctx || this,
     success: function(data, status, jqxhr) {
       callback(data.results);
     }
    });
};

/**
  * Return N completely random songs.
  *
  * @param {integer} n Number of songs requested
  *
  */
djr.Backend.prototype.randomPlaylist = function(num, callback ,ctx) {
  wrap_with_loader(
    {url: '/json/random',
     data: {'n': num },
     dataType: 'json',
     type: 'GET',
     context: ctx || this,
     success: function(data, status, jqxhr) {
       callback(data.results);
     }
    });
};

