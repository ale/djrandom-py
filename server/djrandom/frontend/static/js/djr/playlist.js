// playlist.js


/**
 * A Playlist chunk (basically, an array of songs).
 *
 * A chunk is the lowest level unit of a playlist, it's just an
 * ordered list of songs, with a title.
 *
 * @constructor
 * @param {Array[string]} songs SHA1 hashes of the songs.
 * @param {string} title Chunk title (optional).
 */
djr.PlaylistChunk = function(songs, title) {
  this.songs = songs || [];
  this.title = title;
};

/**
 * Check if the playlist contains a specific song.
 *
 * @param {string} sha1 Song SHA1 hash.
 * @return {bool} True if the song is in this chunk.
 */
djr.PlaylistChunk.prototype.hasSong = function(sha1) {
  return (this.songs.indexOf(sha1) >= 0);
};

/**
 * Remove a song from the playlist (if present).
 *
 * @param {string} sha1 Song SHA1 hash.
 */
djr.PlaylistChunk.prototype.removeSong = function(sha1) {
  this.songs = $.grep(this.songs, function(a) { return a != sha1; });
};

// HTML template for control buttons, generated from JS.
djr.controlButtons = function(id_base) {
  return ('<div class=\"ctlbox\" style=\"display:none\">' 
          + '<a id=\"' + id_base + '_remove\" class="ctl_btn ctl_remove">&nbsp;</a>'
          + '</div>');
};

/**
 * Generate the HTML chunk for the playlist.
 *
 * This method needs a bit of context: when we need to create a new
 * chunk on-the-fly, the HTML fargment returned by the backend search
 * must be wrapped by the chunk <div>.  We do it here in this
 * function, since there's not much to it anyway, and we can save a
 * round-trip to the server when creating a chunk with existing data
 * (as it happens when merging after search, for example).
 *
 * @param {string} chunk_id The chunk ID.
 * @param {string} songs_html The HTML fragment returned by the backend.
 */
djr.PlaylistChunk.prototype.wrapHtml = function(chunk_id, songs_html) {
  return ('<div id=\"chunk_' + chunk_id + '\" class=\"chunk\">'
          + '<div class=\"chunk_ctl_wrap\">'
          + djr.controlButtons('chunk_ctl_' + chunk_id)
          + '<a class=\"chunk_title\">' + this.title + '</a>'
          + '</div>'
          + '<div class=\"chunk_inner\">'
          + (songs_html || '') + '</div></div>');
};



// Playlist.

djr.Playlist = function(uuid) {
  this.uuid = uuid || djr.generateRandomId();
  this.chunks = [];
  this.song_map = {};
  this.chunk_map = {};
  this.title = '';
  this.next_chunk_id = 0;
};

/**
 * Return true if the playlist is empty.
 */
djr.Playlist.prototype.isEmpty = function() {
  return (this.chunks.length == 0);
};

/**
 * Return an array with all songs in the playlist.
 *
 * Songs are returned in order.
 *
 * @return {Array[string]} SHA1 hashes of all songs.
 */
djr.Playlist.prototype.allSongs = function() {
  var songs = [], i, j;
  for (i = 0; i < this.chunks.length; i++) {
    for (j = 0; j < this.chunk_map[this.chunks[i]].songs.length; j++) {
      songs.push(this.chunk_map[this.chunks[i]].songs[j]);
    }
  }
  return songs;
};

/**
 * Creates a new chunk with only unique songs.
 *
 * This method creates a new chunk that enforces the constraint that a
 * playlist should only contain unique songs. The newly created chunk
 * is just returned, but not added to the playlist.
 *
 * @param {Array[string]} songs SHA1 hashes of the songs.
 * @param {string} title Title of the new chunk.
 * @return {PlaylistChunk} Newly created chunk with unique songs, or
 *     null if the chunk would have been empty.
 */
djr.Playlist.prototype.createUniqueChunk = function(songs, title) {
  var unique = [], i;
  for (i = 0; i < songs.length; i++) {
    var sha1 = songs[i];
    if (!sha1) {
      continue;
    }
    if (this.song_map[sha1] != null) {
      continue;
    }
    unique.push(sha1);
  }
  if (unique.length > 0) {
    return new djr.PlaylistChunk(unique, title);
  } else {
    return null;
  }
};

/**
 * Add a new chunk (only adds unique songs).
 *
 * Also assigns a new unique chunk ID (an integer at the moment).
 *
 * @param {PlaylistChunk} playlist_chunk Chunk to add.
 * @return {number} The chunk ID, or -1 if no songs were added.
 */
djr.Playlist.prototype.addChunk = function(playlist_chunk) {
  djr.debug('adding chunk to playlist ' + this.uuid);
  var i, chunk_id = this.next_chunk_id++;
  for (i = 0; i < playlist_chunk.songs.length; i++) {
    var song = playlist_chunk.songs[i];
    this.song_map[song] = chunk_id;
  }
  this.chunk_map[chunk_id] = playlist_chunk;
  this.chunks.push(chunk_id);
  return chunk_id;
};

/**
 * Return the songs for a specific chunk.
 *
 * @param {number} chunk_id ID of the chunk.
 * @return {Array[string]} SHA1 hashes of the songs.
 */
djr.Playlist.prototype.getChunkSongs = function(chunk_id) {
  return this.chunk_map[chunk_id].songs;
};

/**
 * Remove a chunk.
 *
 * @param {number} chunk_id ID of the chunk to remove.
 */
djr.Playlist.prototype.removeChunk = function(chunk_id) {
  djr.debug('removing chunk ' + chunk_id);
  var songs = this.chunk_map[chunk_id].songs, i;
  for (i = 0; i < songs.length; i++) {
    delete this.song_map[songs[i]];
  }
  delete this.chunk_map[chunk_id];
  this.chunks = $.grep(this.chunks, function(cid) { return cid != chunk_id; });
};

/**
 * Remove a song.
 *
 * @param {string} song SHA1 hash of the song.
 * @return {number} Returns the chunk ID if after removing the song,
 *     the chunk was left completely empty, or -1 otherwise.
 */
djr.Playlist.prototype.removeSong = function(song) {
  djr.debug('removing song ' + song);
  var chunk_id = this.song_map[song];
  this.chunk_map[chunk_id].removeSong(song);
  delete this.song_map[song];
  if (this.chunk_map[chunk_id].songs.length == 0) {
    this.removeChunk(chunk_id);
    return chunk_id;
  } else {
    return -1;
  }
};

/**
 * Merge all chunks into a new playlist.
 *
 * The new Playlist will contain a single chunk, with all the
 * playlists' songs in it, and with a title that created combining the
 * titles of all the existing chunks.
 *
 * The UUID of the new playlist will be the same as the current one.
 *
 * @param {string} title Title for the new chunk (optional).
 * @return {Playlist} The new playlist.
 */
djr.Playlist.prototype.merge = function(title) {
  var new_title_parts = [], i;
  for (i = 0; i < this.chunks.length; i++) {
    new_title_parts.push(this.chunk_map[this.chunks[i]].title);
  }
  var new_title = title;
  if (!new_title) {
    new_title = new_title_parts.join(' + ');
  }
  var new_playlist = new djr.Playlist();
  new_playlist.uuid = this.uuid;
  new_playlist.addChunk(
    new djr.PlaylistChunk(this.allSongs(), new_title));
  return new_playlist;
};

/**
 * Find the next song.
 *
 * @param {string} song SHA1 hash of a song.
 * @return {string} SHA1 hash of the song that comes after the
 *     specified one.
 */
djr.Playlist.prototype.getNextSong = function(song) {
  var cur_chunk = this.song_map[song];
  var chunk_songs = this.chunk_map[cur_chunk].songs;

  var chunk_idx = this.chunks.indexOf(cur_chunk);
  var idx = chunk_songs.indexOf(song) + 1;

  if (idx >= chunk_songs.length) {
    idx = 0;
    chunk_idx++;
    if (chunk_idx >= this.chunks.length) {
      chunk_idx = 0;
    }
  }

  return this.chunk_map[this.chunks[chunk_idx]].songs[idx];
};

djr.Playlist.prototype.isLastSong = function(song) {
  var cur_chunk = this.song_map[song];
  var chunk_songs = this.chunk_map[cur_chunk].songs;
  var chunk_idx = this.chunks.indexOf(cur_chunk);
  var song_idx = chunk_songs.indexOf(song);
  var last_chunk = this.chunks.length - 1;
  var last_song = chunk_songs.length - 1;
  if (chunk_idx == last_chunk && song_idx == last_song) {
    return true;
  } else {
    return false;
  }
};

