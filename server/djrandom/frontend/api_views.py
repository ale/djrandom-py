import urllib
from datetime import datetime
from flask import Flask, request, Response, abort, jsonify, render_template, g
from flask.helpers import send_file
from djrandom import utils
from djrandom.database import Session
from djrandom.model.mp3 import MP3, PlayLog, SearchLog
from djrandom.model.playlist import Playlist
from djrandom.frontend import app, svcs, require_auth
from sqlalchemy import distinct


@app.route('/json/artists')
@require_auth
def all_artists_json():
    artists = [x[0] for x in Session.query(distinct(MP3.artist)) if x[0]]
    return jsonify(artists=artists)


@app.route('/json/albums/<artist>')
@require_auth
def artist_albums_json(artist):
    print 'artist: %s' % repr(artist)
    albums = [x[0] for x in 
              Session.query(distinct(MP3.album)).filter_by(artist=artist)]
    return jsonify(albums=albums)


@app.route('/json/album/<artist>/<album>')
@require_auth
def album_songs_json(artist, album):
    print 'artist, album: %s / %s' % (repr(artist), repr(album))
    songs = [x.to_dict() for x in MP3.get_songs_for_album(artist, album)]
    return jsonify(songs=songs)


@app.route('/json/song/<sha1>')
@require_auth
def song_info_json(sha1):
    mp3 = MP3.query.get(sha1)
    if not mp3:
        abort(404)
    return jsonify(mp3.to_dict())


@app.route('/json/playing', methods=['POST'])
@require_auth
def play_callback():
    sha1 = request.form['cur']
    prev = request.form.get('prev', '').split(',')
    mp3 = MP3.query.get(sha1)
    if not mp3:
        abort(404)
    # Increase play count.
    if mp3.play_count:
        mp3.play_count += 1
    else:
        mp3.play_count = 1
    # Add a playlog entry.
    plog = PlayLog(sha1=sha1, userid=g.userid,
                   stamp=datetime.now(), 
                   prev=','.join(prev))
    Session.add(plog)
    Session.add(mp3)
    Session.commit()
    return jsonify(status=True)


@app.route('/album_image/<artist>/<album>')
@require_auth
def get_album_image(artist, album):
    img_file = svcs['album_images'].get_album_image(
        urllib.unquote(artist),
        urllib.unquote(album))
    if not img_file:
        abort(404)
    return send_file(img_file, mimetype='image/jpeg', conditional=True)


@app.route('/json/playlist/save', methods=['POST'])
@require_auth
def save_playlist():
    hashes = request.form.get('h', '')
    uuid = request.form.get('uuid')
    title = request.form.get('title')
    if uuid == 'null':
        uuid = None
    playlist = None
    if uuid:
        playlist = Playlist.query.get(uuid)
    if not playlist:
        playlist = Playlist(uuid=uuid, userid=g.userid)
    playlist.modified_at = datetime.now()
    playlist.contents = hashes
    playlist.title = title
    Session.add(playlist)
    Session.commit()
    return jsonify(uuid=playlist.uuid, status=True)


@app.route('/json/playlist/stream', methods=['POST'])
@require_auth
def stream_playlist():
    uuid = request.form.get('uuid')
    enable_stream = (request.form.get('stream') == 'y')
    playlist = Playlist.query.get(uuid)
    if not playlist:
        abort(404)
    playlist.streaming = enable_stream
    Session.add(playlist)
    Session.commit()
    data = {'stream': playlist.streaming}
    if playlist.streaming:
        data['url'] = playlist.stream_url()
    return jsonify(data)


@app.route('/json/playlist/get/<uuid>')
@require_auth
def playlist_info_json(uuid):
    playlist = Playlist.query.get(uuid)
    if not playlist:
        abort(404)
    return jsonify(playlist.to_dict())


@app.route('/json/playlist/by_title/<userid>/<title>')
@require_auth
def playlist_info_by_title_json(userid, title):
    playlist = Playlist.get_by_title(userid, title)
    if not playlist:
        abort(404)
    return jsonify(playlist.to_dict())


@app.route('/json/playlist/list/<userid>')
@require_auth
def playlist_list_by_uuid_json(userid):
    playlists = [x.to_dict() for x in Playlist.get_all_by_user(userid)]
    return jsonify(results=playlists)


@app.route('/json/search')
@require_auth
def search_json():
    query = request.args.get('q')
    num_results = int(request.args.get('n', 100))
    results = []
    if query:
        resultiter = svcs['searcher'].search(query, n=num_results)
        results = [{'score': score, 'sha1': sha1}
                   for (score, sha1) in resultiter]
        if results:
            # Save this query in the search log.
            Session.add(SearchLog.add(query, g.userid))
            Session.commit()
    return jsonify(results=results)


@app.route('/json/morelikethese', methods=['POST'])
@require_auth
def more_like_these_json():
    hashes = request.form.get('h', '').split(',')
    n = int(request.form.get('n', 5))
    results = []
    if hashes:
        results = svcs['searcher'].more_like_these(hashes, n)
    return jsonify(results=results)


@app.route('/json/most_played', methods=['GET'])
@require_auth
def most_played_json():
    n = int(request.args.get('n', 20))
    most_played = [{'sha1': sha1, 'count': count}
                   for sha1, count in PlayLog.most_played(n)]
    return jsonify(results=most_played)


@app.route('/json/never_played', methods=['GET'])
@require_auth
def never_played_json():
    n = int(request.args.get('n', 20))
    never_played = MP3.never_played(n)
    return jsonify(results=never_played)


@app.route('/json/last_uploaded', methods=['GET'])
@require_auth
def last_uploaded_json():
    n = int(request.args.get('n', 20))
    last_uploaded = [x.sha1 for x in MP3.last_uploaded(n)]
    return jsonify(results=last_uploaded)


@app.route('/json/markov', methods=['POST'])
@require_auth
def markov_json():
    n = int(request.form.get('n', 10))
    hashes = request.form.get('h', '').split(',')
    last_songs = hashes[-1:]
    sequence = svcs['markov'].generate_sequence(last_songs, 1, n)
    return jsonify(results=sequence)


@app.route('/json/markov_vector', methods=['GET'])
@require_auth
def markov_vector_json():
    hashes = request.args.get('h', '').split(',')
    last_songs = hashes[-1:]
    sequence = svcs['markov'].get_vector(last_songs)
    return jsonify(results=sequence)


@app.route('/json/random', methods=['GET'])
@require_auth
def random_json():
    n = int(request.args.get('n', 10))
    random_songs = MP3.get_random_songs(n)
    return jsonify(results=random_songs)
