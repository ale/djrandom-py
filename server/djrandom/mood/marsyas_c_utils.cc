
#include <marsyas/NumericLib.h>
#include "marsyas_c_utils.h"

using namespace Marsyas;

double euclidean_distance(const realvec& a, const realvec& b)
{
  realvec cov;
  return NumericLib::euclideanDistance(a, b, cov);
}

