import marsyas
import os
import tempfile


class mp3_wrapper(object):

    def __init__(self, path):
        self._path = path

    def __enter__(self):
        self._tmpdir = tempfile.mkdtemp()
        self._link = os.path.join(self._tmpdir, self._path.replace('/', '_') + '.mp3')
        os.symlink(self._path, self._link)
        return self._link

    def __exit__(self, *args):
        os.unlink(self._link)
        os.rmdir(self._tmpdir)


def _vector_from_file(path):
    mng = marsyas.MarSystemManager()
    fnet = mng.create('Series', 'fnet')
    fnet.addMarSystem(mng.create('SoundFileSource', 'src'))
    fnet.addMarSystem(mng.create('DownSampler', 'ds'))
    fnet.addMarSystem(mng.create('Stereo2Mono', 's2m'))

    fnet.addMarSystem(mng.create('TimbreFeatures', 'tf'))

    fnet.updControl('TimbreFeatures/tf/mrs_string/enableTDChild', 'ZeroCrossings/zcrs')
    fnet.updControl('TimbreFeatures/tf/mrs_string/enableSPChild', 'MFCC/mfcc')
    fnet.updControl('TimbreFeatures/tf/mrs_string/enableSPChild', 'Centroid/cntrd')
    fnet.updControl('TimbreFeatures/tf/mrs_string/enableSPChild', 'Flux/flux')
    fnet.updControl('TimbreFeatures/tf/mrs_string/enableSPChild', 'Rolloff/rlf')

    # Add the texture statistics
    fnet.addMarSystem(mng.create('TextureStats', 'tStats'))

    # Set the texture memory size to a 1-second view (22 frames).
    fnet.updControl('TextureStats/tStats/mrs_natural/memSize', 22)

    # Set the input file name.
    fnet.updControl('SoundFileSource/src/mrs_string/filename', path)

    # Downsample the input audio to 11250Hz.
    tmp = fnet.getControl('SoundFileSource/src/mrs_real/osrate').to_real()
    factor = int(round(tmp / 11250.0))
    fnet.updControl('DownSampler/ds/mrs_natural/factor', factor)

    # Set the window to 1024 samples at 11250 Hz
    # blah
    fnet.updControl("TimbreFeatures/tf/Series/timeDomain/ShiftInput/si/mrs_natural/winSize", 1024)
    fnet.updControl("TimbreFeatures/tf/Series/spectralShape/ShiftInput/si/mrs_natural/winSize", 1024)
    fnet.updControl("TimbreFeatures/tf/Series/lpcFeatures/ShiftInput/si/mrs_natural/winSize", 1024)
    
    # Find the length of the song.
    slength = fnet.getControl('SoundFileSource/src/mrs_natural/size').to_natural()
    numsamps = (((30 * 11250 * factor) / 512) * 512)
    start = (slength - numsamps) / 2

    fnet.updControl('SoundFileSource/src/mrs_natural/pos', start)
    fnet.updControl('SoundFileSource/src/mrs_natural/onSamples', numsamps)

    acc = mng.create('Accumulator', 'acc')
    acc.updControl('mrs_natural/nTimes', int((30 * 11025.0) / 512))

    sstats = mng.create('Fanout', 'sstats')
    sstats.addMarSystem(mng.create('Mean', 'smn'))
    sstats.addMarSystem(mng.create('StandardDeviation', 'sstd'))

    tnet = mng.create('Series', 'tnet')
    acc.addMarSystem(fnet)
    tnet.addMarSystem(acc)
    tnet.addMarSystem(sstats)
    tnet.updControl('mrs_natural/inSamples', factor * 512)

    tnet.tick()

    return tnet.getControl('mrs_realvec/processedData').to_realvec()


def vector_from_file(path):
    with mp3_wrapper(path) as mp3_path:
        return _vector_from_file(mp3_path)


def main():
    import sys, marsyas_utils

    result = vector_from_file(sys.argv[1])
    rstr = marsyas_utils.serialize_realvec(result)
    sys.stdout.write(rstr)


if __name__ == '__main__':
    main()
