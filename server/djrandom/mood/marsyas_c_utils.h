// -*- c++ -*-

#ifndef __djrandom_mood_H
#define __djrandom_mood_H 1

#include <marsyas/realvec.h>

double euclidean_distance(const Marsyas::realvec& a,
                          const Marsyas::realvec& b);

#endif
