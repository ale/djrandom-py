import os
import optparse
import logging
import subprocess
import time
import traceback
from djrandom import daemonize
from djrandom import utils
from djrandom.model.mp3 import MP3
from djrandom.database import Session, init_db
from djrandom.model import processor

log = logging.getLogger(__name__)


# We run extract_features as an external program because Marsyas has
# a nasty tendency to leak memory during analysis...
def get_features(path):
    pipe = subprocess.Popen(['djrandom-mood-extract-features', path],
                            stdout=subprocess.PIPE)
    return pipe.communicate()[0]


class TimbreFeatureExtractor(processor.Processor):

    def process(self, mp3):
        log.info('extracting features from %s' % mp3.sha1)
        try:
            vector_str = get_features(mp3.path)
        except Exception, e:
            log.error('error processing %s: %s' % (mp3.sha1, e))
            return
        mp3.set_features(timbre_vector=vector_str)

    def query(self):
        return MP3.get_with_no_features()


def run_feature_extractor(db_url, run_once):
    init_db(db_url)
    scanner = TimbreFeatureExtractor()
    scanner.loop(run_once=run_once)


def main():
    parser = optparse.OptionParser()
    parser.add_option('--once', action='store_true')
    parser.add_option('--db_url')
    daemonize.add_standard_options(parser)
    utils.read_config_defaults(
        parser, os.getenv('DJRANDOM_CONF', '/etc/djrandom.conf'))
    opts, args = parser.parse_args()
    if not opts.db_url:
        parser.error('Must provide --db_url')
    if args:
        parser.error('Too many arguments')

    if opts.once:
        opts.foreground = True

    daemonize.daemonize(opts, run_feature_extractor,
                        (opts.db_url, opts.once))


if __name__ == '__main__':
    main()
