import marsyas
import cPickle as pickle


def serialize_realvec(rv):
    rows = rv.getRows()
    data = [rv[i] for i in xrange(rows)]
    return pickle.dumps((rows, data),
                        pickle.HIGHEST_PROTOCOL)


def deserialize_realvec(data):
    rows, data = pickle.loads(data)
    r = marsyas.realvec(rows, 1)
    for i in xrange(rows):
        r[i] = data[i]
    return r

