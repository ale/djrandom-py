import mox
import os
import shutil
import solr
import tempfile
from djrandom.model import indexer
from djrandom import database


class DbTestCase(mox.MoxTestBase):

    def setUp(self):
        mox.MoxTestBase.setUp(self)
        self.tmpdir = tempfile.mkdtemp()
        self.mox.StubOutWithMock(indexer.Indexer, 'add_mp3')
        self.mox.StubOutWithMock(indexer.Indexer, 'del_mp3')
        self.mox.StubOutWithMock(indexer.Indexer, 'commit')
        self.engine = database.init_db(
            'sqlite:///%s' % os.path.join(self.tmpdir, 'data.db'),
            'http://solr/')

    def tearDown(self):
        mox.MoxTestBase.tearDown(self)
        database.Session.remove()
        database.Base.metadata.drop_all(self.engine)
        shutil.rmtree(self.tmpdir)


class SolrTestCase(mox.MoxTestBase):
    """A more complex base test class, with temporary storage and SOLR."""

    def setUp(self):
        mox.MoxTestBase.setUp(self)
        # Do not use StubOutWithMock, since we don't know if the test is
        # actually going to initialize the indexer or not.
        self.solr = self.mox.CreateMock(solr.Solr)
        self.old_solr = solr.Solr
        solr.Solr = lambda url, **kw: self.solr
        database.init_db('sqlite://', 'http://solr/')

    def tearDown(self):
        database.Session.remove()
        # Reset database.indexer
        database.indexer._solr = None
        solr.Solr = self.old_solr
        mox.MoxTestBase.tearDown(self)
    

class WsgiTestCase(DbTestCase):

    FLASK_APP = None

    def setUp(self):
        DbTestCase.setUp(self)
        self.FLASK_APP.config['TESTING'] = True
        self.app = self.FLASK_APP.test_client()

    def _load_data(self, data):
        sess = database.Session()
        for item in data:
            print 'adding item', str(item)
            sess.add(item)
        sess.commit()


