import hashlib
import json
from djrandom.test import WsgiTestCase
from djrandom.receiver import receiver
from djrandom.model.mp3 import MP3


class ReceiverTest(WsgiTestCase):

    FLASK_APP = receiver.app

    def setUp(self):
        WsgiTestCase.setUp(self)
        receiver.storage_root = self.tmpdir
        self._load_data([MP3(sha1='1234')])

    def test_check_existing(self):
        rv = self.app.get('/check/1234')
        result = json.loads(rv.data)
        self.assertEquals({'status': True}, result)

    def test_check_nonexisting(self):
        rv = self.app.get('/check/2345')
        result = json.loads(rv.data)
        self.assertEquals({'status': False}, result)

    def test_upload_ok(self):
        filedata = 'buzz buzz buzz'
        filesha1 = hashlib.sha1(filedata).hexdigest()
        rv = self.app.post('/upload/%s' % filesha1,
                           content_type='audio/mpeg',
                           data=filedata)
        result = json.loads(rv.data)
        self.assertEquals({'status': True}, result)

        # Check that the MP3 exists and it's in the right state
        mp3 = MP3.query.get(filesha1)
        self.assertTrue(mp3 is not None)
        self.assertEquals(MP3.INCOMING, mp3.state)

        # Check that the file contents have been saved properly
        with open(mp3.path, 'r') as fd:
            actual_data = fd.read()
        self.assertEquals(filedata, actual_data)

    def test_upload_sha1_mismatch(self):
        filedata = 'buzz buzz buzz'
        filesha1 = 'wrongsha1'
        rv = self.app.post('/upload/%s' % filesha1,
                           content_type='audio/mpeg',
                           data=filedata)
        self.assertEquals(400, rv.status_code)

    def test_upload_wrong_content_type(self):
        filedata = 'buzz buzz buzz'
        filesha1 = hashlib.sha1(filedata).hexdigest()
        rv = self.app.post('/upload/%s' % filesha1,
                           data=filedata)
        self.assertEquals(400, rv.status_code)
