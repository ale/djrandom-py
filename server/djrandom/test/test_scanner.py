import eyeD3
import hashlib
import json
from djrandom.database import Session, indexer
from djrandom.test import DbTestCase
from djrandom.scanner import scanner
from djrandom.scanner import metadata
from djrandom.model.mp3 import MP3


class FakeGenre(object):

    def __init__(self, name):
        self.name = name

    def getName(self):
        return self.name


class ScannerTest(DbTestCase):

    def setUp(self):
        DbTestCase.setUp(self)
        data = [
            {'sha1': '1', 'state': MP3.INCOMING, 'path': '/root/1'},
            {'sha1': '2', 'state': MP3.READY, 'path': '/root/2',
             'artist': u'artist', 'title': u'title'},
            {'sha1': '3', 'state': MP3.ERROR, 'path': '/root/3'},
            ]
        for attrs in data:
            mp3 = MP3(**attrs)
            Session.add(mp3)
        Session.commit()
    
    def test_scanner_process_good_metadata(self):
        self.mox.StubOutWithMock(metadata, 'analyze_mp3')
        metadata.analyze_mp3('/root/1').AndReturn({
                'artist': u'artist', 'title': u'title'})
        self.mox.ReplayAll()

        mp3 = MP3.query.get('1')
        sc = scanner.Scanner()
        sc._process(mp3)
        self.assertEquals(u'artist', mp3.artist)
        self.assertEquals(u'title', mp3.title)

    def test_scanner_process_bad_metadata(self):
        self.mox.StubOutWithMock(metadata, 'analyze_mp3')
        metadata.analyze_mp3('/root/1').AndReturn({})
        self.mox.ReplayAll()

        mp3 = MP3.query.get('1')
        sc = scanner.Scanner()
        self.assertRaises(scanner.BadMetadataError,
                          sc._process, mp3)

    def test_scanner_run(self):
        sc = scanner.Scanner()
        mp3 = MP3.query.get('1')

        self.mox.StubOutWithMock(sc, '_process')
        sc._process(mp3)
        indexer.add_mp3(mp3)
        indexer.commit()

        self.mox.ReplayAll()

        sc.loop(run_once=True)

        # Verify changes to the mp3 object.
        mp3b = MP3.query.get('1')
        self.assertEquals(MP3.READY, mp3b.state)

    def test_scanner_run_bad_metadata(self):
        sc = scanner.Scanner()
        mp3 = MP3.query.get('1')

        self.mox.StubOutWithMock(sc, '_process')
        sc._process(mp3).AndRaise(scanner.BadMetadataError())
        indexer.add_mp3(mp3)
        indexer.commit()

        self.mox.ReplayAll()

        sc.loop(run_once=True)

        # Verify changes to the mp3 object.
        mp3b = MP3.query.get('1')
        self.assertEquals(MP3.BAD_METADATA, mp3b.state)

    def test_scanner_run_error(self):
        sc = scanner.Scanner()
        mp3 = MP3.query.get('1')

        self.mox.StubOutWithMock(sc, '_process')
        sc._process(mp3).AndRaise(Exception('something bad!'))
        indexer.add_mp3(mp3)
        indexer.commit()

        self.mox.ReplayAll()

        sc.loop(run_once=True)

        # Verify changes to the mp3 object.
        mp3b = MP3.query.get('1')
        self.assertEquals(MP3.ERROR, mp3b.state)

    def test_metadata_normalize_string(self):
        testdata = [
            (None, None),
            ('', ''),
            (u'a', u'a'),
            (u'perep\xe8', u'perep\xe8'),
            (u'bla bla_BLA$$$', u'bla bla bla'),
            ]
        for src, expected in testdata:
            result = metadata.normalize_string(src)
            self.assertEquals(expected, result)

    def test_metadata_analyze_mp3(self):
        tag = self.mox.CreateMock(eyeD3.Tag)
        self.mox.StubOutWithMock(eyeD3, 'Tag', use_mock_anything=True)
        eyeD3.Tag().AndReturn(tag)
        tag.link('/path/to/mp3')
        tag.getGenre().InAnyOrder().AndReturn(FakeGenre(u'genre'))
        tag.getArtist().InAnyOrder().AndReturn(u'artist')
        tag.getAlbum().InAnyOrder().AndReturn(u'album')
        tag.getTitle().InAnyOrder().AndReturn(u'title')
        tag.getTrackNum().InAnyOrder().AndReturn((1, 10))

        self.mox.ReplayAll()
        result = metadata.analyze_mp3('/path/to/mp3')

        self.assertEquals(
            {'artist': u'artist',
             'album': u'album',
             'title': u'title',
             'genre': u'genre',
             'track_num': 1
             },
            result)
