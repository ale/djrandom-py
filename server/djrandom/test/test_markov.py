import os
from djrandom.test import DbTestCase
from djrandom.model import markov


class MarkovTest(DbTestCase):

    def setUp(self):
        DbTestCase.setUp(self)

        # Build a simple n=2 model.
        self.source = [
            ('1', [None]),
            ('1', ['4']),
            ('2', ['1']),
            ('3', ['2']),
            ('4', ['3']),
            ('4', ['1']),
            ]
        self.markov = markov.MarkovModel()
        self.markov.create(self.source)
        self.markov.normalize()

    def test_markov_create_ok(self):
        # Test some internals.
        self.assertEquals(5, len(self.markov._hash2i))
        self.assertEquals(5, len(self.markov._i2hash))

    def test_markov_suggest_single(self):
        # There is just one possible answer.
        self.assertEquals('3', self.markov.suggest(['2']))
        self.assertEquals('1', self.markov.suggest([None]))

    def test_markov_suggest_many(self):
        # There is more than one possibility for '1', so run a bunch of
        # queries and count the results.
        counts = {'2': 0, '4': 0}
        for i in range(1000):
            result = self.markov.suggest(['1'])
            self.assertTrue(result in ('2', '4'),
                            'unexpected random result "%s"' % result)
            counts[result] += 1
        ratio = float(counts['2']) / float(counts['4'])
        if ratio < 1:
            ratio = 1.0 / ratio
        # Be tolerant.
        self.assertTrue(ratio < 1.5,
                        'unbalanced distribution: %s' % str(counts))

    def test_markov_suggest_non_existing(self):
        for i in xrange(100):
            # '5' should not exist.
            result = self.markov.suggest(['5'])
            self.assertTrue(result in ('1', '2', '3', '4', '5'),
                            'unexpected random result "%s"' % result)

    def test_markov_generate_sequence(self):
        result = self.markov.generate_sequence(['2'], 1, 2)
        self.assertEquals(['3', '4'], result)

    def test_markov_save_and_reload(self):
        filename = os.path.join(self.tmpdir, 'markov.dat')
        self.markov.save(filename)
        self.assertTrue(os.path.exists(filename))

        markov_b = markov.MarkovModel()
        markov_b.load(filename)

        self.assertEquals(self.markov._hash2i, markov_b._hash2i)
        self.assertEquals(self.markov._i2hash, markov_b._i2hash)
        self.assertEquals(self.markov._map, markov_b._map)
