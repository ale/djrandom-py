import mox
import os
import shutil
import tempfile
import unittest
from djrandom import utils
from djrandom.test import DbTestCase


class UtilsTest(mox.MoxTestBase):

    def setUp(self):
        mox.MoxTestBase.setUp(self)
        self.tmpdir = tempfile.mkdtemp()

    def tearDown(self):
        shutil.rmtree(self.tmpdir)
        mox.MoxTestBase.tearDown(self)

    def test_generate_path(self):
        self.assertEquals('/base/a/b/abcd',
                          utils.generate_path('/base', 'abcd'))

    def test_random_token(self):
        result = utils.random_token()
        self.assertTrue(len(result) > 10)
        result2 = utils.random_token()
        self.assertNotEquals(result, result2)

    def test_sha1_of_file(self):
        test_file = os.path.join(self.tmpdir, 'testfile')
        with open(test_file, 'w') as fd:
            fd.write('test\n')
        sha1 = utils.sha1_of_file(test_file)
        self.assertEquals('4e1243bd22c66e76c2ba9eddc1f91394e57f9f83', sha1)

    def test_read_config_defaults(self):
        cfg_file = os.path.join(self.tmpdir, 'config')
        with open(cfg_file, 'w') as fd:
            fd.write('''
# Test config file
var_a=a
var_b = b

  var_c  = 42
''')

        parser = self.mox.CreateMockAnything()
        parser.set_default('var_a', 'a')
        parser.set_default('var_b', 'b')
        parser.set_default('var_c', '42')
        self.mox.ReplayAll()

        utils.read_config_defaults(parser, cfg_file)

    def test_read_config_file_with_error(self):
        cfg_file = os.path.join(self.tmpdir, 'config')
        with open(cfg_file, 'w') as fd:
            fd.write('this is not a config\n')

        self.assertRaises(utils.SyntaxError,
                          utils.read_config_defaults,
                          None, cfg_file)

    def test_read_config_file_missing(self):
        utils.read_config_defaults(
            None, os.path.join(self.tmpdir, 'nosuchfile'))
