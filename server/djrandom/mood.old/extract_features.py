import os
import optparse
import logging
import subprocess
import time
import traceback
from djrandom import daemonize
from djrandom import utils
from djrandom.model.mp3 import MP3
from djrandom.database import Session, init_db
from djrandom.model import processor
from djrandom.mood import mood

log = logging.getLogger(__name__)


def vector_from_file(path):
    ln = os.path.join('/tmp', '__analysis_%s.mp3' % os.path.basename(path))
    os.symlink(path, ln)
    try:
        return mood.vector_from_file(ln)
    finally:
        os.unlink(ln)


class FeatureExtractor(processor.Processor):

    def process(self, mp3):
        log.info('extracting features from %s' % mp3.sha1)
        try:
            timbre_vector = vector_from_file(mp3.path)
        except Exception, e:
            log.error('error processing %s: %s' % (mp3.sha1, e))
            return
        mp3.set_features(timbre_vector=timbre_vector)

    def query(self):
        return MP3.get_with_no_features()


def run_feature_extractor(db_url, run_once):
    init_db(db_url)
    scanner = FeatureExtractor()
    scanner.loop(run_once=run_once)


def main():
    parser = optparse.OptionParser()
    parser.add_option('--once', action='store_true')
    parser.add_option('--db_url')
    daemonize.add_standard_options(parser)
    utils.read_config_defaults(
        parser, os.getenv('DJRANDOM_CONF', '/etc/djrandom.conf'))
    opts, args = parser.parse_args()
    if not opts.db_url:
        parser.error('Must provide --db_url')
    if args:
        parser.error('Too many arguments')

    if opts.once:
        opts.foreground = True

    daemonize.daemonize(opts, run_feature_extractor,
                        (opts.db_url, opts.once))


if __name__ == '__main__':
    main()
