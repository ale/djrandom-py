%module mood
%include "std_vector.i"
%include "std_string.i"

%{
#define SWIG_FILE_WITH_INIT
#include "mood.h"
%}

%typemap(in) (const std::map<std::string, Marsyas::realvec>& vectors) {

  // Check type.
  if (!PyDict_Check($input)) {
    PyErr_SetString(PyExc_TypeError, "Not a dict");
    return NULL;
  }

  $1 = new mood::vector_map_t;

  // Iterate on the Python dictionary and build the unserialized
  // representation.
  PyObject *key, *value;
  Py_ssize_t pos = 0;
  while (PyDict_Next($input, &pos, &key, &value)) {
    $1->insert(
      std::make_pair(
        PyString_AsString(key),
        mood::vector_from_string(PyString_AsString(value))));
  }

}

%include "mood.h"

