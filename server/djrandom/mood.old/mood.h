// -*- c++ -*-

#ifndef __djrandom_mood_H
#define __djrandom_mood_H 1

#include <map>
#include <vector>
#include <string>
#include <marsyas/realvec.h>

namespace mood {

typedef std::map<std::string, Marsyas::realvec> vector_map_t;

class Mood
{
public:
  Mood(const vector_map_t& vectors)
    : vectors_(vectors)
  {}

  virtual ~Mood()
  {}

  std::vector<std::string> compare(const std::string& ref_key,
                                   const std::string& ref_vector_str,
                                   int n);

protected:
  vector_map_t vectors_;
};

// Extract a (serialized) vector from an MP3 file.
std::string vector_from_file(const std::string& filename);

// Internal serialization functions.
std::string vector_to_string(const Marsyas::realvec& v);
Marsyas::realvec vector_from_string(const std::string& vstr);


} // namespace mood

#endif
