from distutils.core import setup, Extension

mood_module = Extension(
    '_mood',
    sources=['mood_wrap.cc', 'mood.cc'],
    libraries=['marsyas'],
    )

setup(name='mood',
      version='0.1',
      author='ale@incal.net',
      description='suchen',
      ext_modules=[mood_module],
      py_modules=['mood'],
      )
