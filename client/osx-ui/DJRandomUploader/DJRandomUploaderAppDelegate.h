//
//  DJRandomUploaderAppDelegate.h
//
//  Created by <ale@incal.net> on 16/10/2011.
//  Copyright 2011 ale@incal.net. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "ProcessController.h"

@interface DJRandomUploaderAppDelegate : NSObject <NSApplicationDelegate> {
@private
    NSWindow *window;
    IBOutlet NSWindow *prefsWindow;

    NSTimer *statusTimer;

    NSImage *menuIcon;
    IBOutlet NSMenu *statusMenu;
    IBOutlet NSMenuItem *statusMenuItem;
    IBOutlet NSTextField *bwLimitField;
    IBOutlet NSNumberFormatter *bwLimitFormatter;
    IBOutlet NSTextField *musicFolderField;
    IBOutlet NSTextField *apiKeyField;
    IBOutlet NSMenu *uploadBwMenu;
    
    NSStatusItem *statusItem;

    NSUserDefaults *preferences;
    ProcessController *daemon;
}

@property (assign) IBOutlet NSWindow *window;

@end
