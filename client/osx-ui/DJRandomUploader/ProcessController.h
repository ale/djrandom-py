//
//  ProcessController.h
//
//  Created by <ale@incal.net> on 16/10/2011.
//  Copyright 2011 ale@incal.net. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface ProcessController : NSObject {
@private
    NSTask *task;
    NSString *path;
    NSString *curMusicFolder;
    NSString *curApiKey;
    NSNumber *curBwLimit;
}

- (BOOL)isRunning;
- (void)start:(NSString *)musicDir apiKey:(NSString *)apiKey bwLimit:(NSNumber *)bwLimit;
- (void)stop;
- (NSDictionary*)readState;

@end
