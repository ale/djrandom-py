//
//  DJRandomUploaderAppDelegate.m
//
//  Created by <ale@incal.net> on 16/10/2011.
//  Copyright 2011 ale@incal.net. All rights reserved.
//

#import "DJRandomUploaderAppDelegate.h"

@implementation DJRandomUploaderAppDelegate

@synthesize window;

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    // Insert code here to initialize your application
}

- (void)dealloc {
    [menuIcon release];
    [daemon release];
}

- (void)awakeFromNib
{
    NSBundle *bundle = [NSBundle bundleForClass:[self class]];
    NSString *path = [bundle pathForResource:@"pirate-icon-22px" ofType:@"png"];
    menuIcon= [[NSImage alloc] initWithContentsOfFile:path];
    
    statusItem = [[[NSStatusBar systemStatusBar] statusItemWithLength:NSVariableStatusItemLength] retain];
    [statusItem setMenu:statusMenu];
    [statusItem setHighlightMode:YES];
    [statusItem setTitle:[NSString stringWithString:@""]];     
    [statusItem setImage:menuIcon];

    [statusMenuItem setTitle:@"Not Running"];

    // Recover saved preferences.
    preferences = [[NSUserDefaults standardUserDefaults] retain];
    NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:
                          @"~/Music", @"musicFolder",
                          @"", @"apiKey",
                          [NSNumber numberWithInt:0], @"bwLimit",
                          nil];
    [preferences registerDefaults:dict];
    
    // Set the saved preferences as the UI defaults.
    NSString *savedMusicFolder = [preferences objectForKey:@"musicFolder"];
    NSString *savedApiKey = [preferences objectForKey:@"apiKey"];
    NSNumber *savedBwLimit = [preferences objectForKey:@"bwLimit"];
    [musicFolderField setStringValue:savedMusicFolder];
    [apiKeyField setStringValue:savedApiKey];
    [bwLimitField setStringValue:[bwLimitFormatter stringFromNumber:savedBwLimit]];
    
    // Start background daemon.
    daemon = [[ProcessController alloc] init];
    [daemon start:savedMusicFolder apiKey:savedApiKey bwLimit:savedBwLimit];

    // Start a timer that will read state and update the tooltip.
    statusTimer = [NSTimer scheduledTimerWithTimeInterval:5.0 
                 target:self 
                 selector:@selector(fireTimer:)
                 userInfo:nil
                 repeats:YES];
}

- (NSApplicationTerminateReply)applicationShouldTerminate:(NSApplication *)sender {
    // Cleanup before exiting.
    [statusTimer invalidate];
    [daemon stop];
    return NSTerminateNow;
}

- (void)fireTimer:(NSTimer*)theTimer {
    NSString *message = @"Error";

    if ([daemon isRunning]) {
        NSDictionary *state = [daemon readState];
        if (state != nil) {
            if ([state valueForKey:@"uploading"] == [NSNull null]) {
                message = @"Idle";
            } else {
                NSString *currentlyUploadingStr = [state valueForKey:@"uploading"];
                message = [NSString stringWithFormat:@"Uploading: %@",
                                     [currentlyUploadingStr lastPathComponent]];
            }
        } else {
            message = @"Initializing...";
        }
    } else {
        message = @"Not Running";
    }

    [statusMenuItem setTitle:message];
    // NSLog(@"timer: %@", message);
}

-(IBAction)chooseMusicFolder:(id)sender{
    int i; // Loop counter.
    
    // Create the File Open Dialog class.
    NSOpenPanel* openDlg = [NSOpenPanel openPanel];
    
    // Enable the selection of files in the dialog.
    [openDlg setCanChooseFiles:YES];
    
    // Enable the selection of directories in the dialog.
    [openDlg setCanChooseDirectories:YES];
    
    // Display the dialog.  If the OK button was pressed,
    // process the files.
    if ( [openDlg runModalForDirectory:nil file:nil] == NSOKButton )
    {
        // Get an array containing the full filenames of all
        // files and directories selected.
        NSArray* files = [openDlg filenames];
        
        // Loop through all the files and process them.
        for( i = 0; i < [files count]; i++ )
        {
            NSString* fileName = [files objectAtIndex:i];
            
            // Do something with the filename.
            [musicFolderField setStringValue:fileName];
        }
    }
}

-(IBAction)showPreferences:(id)sender{
    NSLog(@"Show preferences window");
    [prefsWindow orderFront:sender];
}

-(IBAction)applyPreferences:(id)sender{
    NSLog(@"Hide preferences window");
    [prefsWindow orderOut:sender];

    NSString *apiKey = [apiKeyField stringValue];
    NSString *musicFolder = [musicFolderField stringValue];
    NSNumber *bwLimit = [bwLimitFormatter numberFromString:[bwLimitField stringValue]];

    // Save preferences.
    [preferences setObject:musicFolder forKey:@"musicFolder"];
    [preferences setObject:apiKey forKey:@"apiKey"];
    [preferences setObject:bwLimit forKey:@"bwLimit"];
    [preferences synchronize];
    
    // Restart daemon process with the new settings.
    [daemon start:musicFolder apiKey:apiKey bwLimit:bwLimit];
}

@end
