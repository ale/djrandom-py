#!/bin/bash -e

PYTHON=${PYTHON:-python}

app_path="${BUILT_PRODUCTS_DIR}/${UNLOCALIZED_RESOURCES_FOLDER_PATH}/../.."
if [ ! -d "${app_path}" ]; then
  echo "Usage: fix-app <PATH OF THE .app BUNDLE>" 1>&2
  exit 1
fi

# Create Py2app bundle.
src_path="${PROJECT_DIR}/.."
(cd "${src_path}" ; ${PYTHON} setup.py py2app) >/dev/null || exit 1
py2app_dir="${src_path}/dist/djrandom-client.app"

for dirname in MacOS Resources Frameworks
do
  echo "rsyncing ${app_path}/Contents/${dirname}" 1>&2
  rsync -av "${py2app_dir}/Contents/${dirname}/" \
    "${app_path}/Contents/${dirname}/"
done

write_plist_snippet() {
cat <<EOF
<key>PyMainFileNames</key>
<array>
<string>__boot__</string>
</array>
<key>PyOptions</key>
<dict>
<key>alias</key>
<false/>
<key>argv_emulation</key>
<false/>
<key>no_chdir</key>
<false/>
<key>optimize</key>
<integer>0</integer>
<key>prefer_ppc</key>
<false/>
<key>site_packages</key>
<false/>
<key>use_pythonpath</key>
<false/>
</dict>
<key>PyResourcePackages</key>
<array>
</array>
<key>PyRuntimeLocations</key>
<array>
<string>@executable_path/../Frameworks/Python.framework/Versions/2.7/Python</string>
</array>
<key>PythonInfoDict</key>
<dict>
<key>PythonExecutable</key>
<string>/usr/bin/python</string>
<key>PythonLongVersion</key>
<string>2.7.1 (r271:86832, Jul 31 2011, 19:30:53) 
[GCC 4.2.1 (Based on Apple Inc. build 5658) (LLVM build 2335.15.00)]</string>
<key>PythonShortVersion</key>
<string>2.7</string>
<key>py2app</key>
<dict>
<key>alias</key>
<false/>
<key>template</key>
<string>app</string>
<key>version</key>
<string>0.5.3</string>
</dict>
</dict>
EOF
}


plistfile="${app_path}/Contents/Info.plist"
(len=$(wc -l "${plistfile}" | awk '{print $1}'); head -n $(($len - 2)) "${plistfile}" ; write_plist_snippet ; tail -2 "${plistfile}") > "${plistfile}.tmp"
mv -f "${plistfile}.tmp" "${plistfile}"

