//
//  main.m
//
//  Created by <ale@incal.net> on 16/10/2011.
//  Copyright 2011 ale@incal.net. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, char *argv[])
{
    return NSApplicationMain(argc, (const char **)argv);
}
