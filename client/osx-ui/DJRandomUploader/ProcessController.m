//
//  ProcessController.m
//
//  Created by <ale@incal.net> on 16/10/2011.
//  Copyright 2011 ale@incal.net. All rights reserved.
//

#import "ProcessController.h"
#import <YAJL/YAJL.h>


@implementation ProcessController

- (id)init
{
    self = [super init];
    if (self) {
        // Initialization code here.
        curMusicFolder = @"";
        curApiKey = @"";
        curBwLimit = [[NSNumber numberWithInt:0] retain];
        task = nil;
        NSBundle *bundle = [NSBundle bundleForClass:[self class]];
        path = [bundle pathForAuxiliaryExecutable:@"djrandom-client"];
        if (path == nil) {
            path = @"/usr/local/bin/djrandom-client";
        }
        NSLog(@"Executable location: %@", path);
    }
    
    return self;
}

- (void)dealloc
{
    [self stop];
    [task dealloc];
    [super dealloc];
}

- (void)stop {
    NSLog(@"Stopping subprocess...");
    if ([task isRunning]) {
        [task terminate];
        [task waitUntilExit];
    }
}

- (void)reloadProcess {
    [self stop];
    if ([curMusicFolder length] == 0 || [curApiKey length] == 0) {
        NSLog(@"Not starting daemon, parameters unset");
    } else {
        NSArray *args = [NSArray 
                         arrayWithObjects:@"--api_key", curApiKey, @"--music_dir", 
                         curMusicFolder, @"--debug", @"--foreground", 
                         nil];
        if ([curBwLimit intValue] > 0) {
            args = [args arrayByAddingObjectsFromArray:[NSArray 
                                                        arrayWithObjects:@"--bwlimit", 
                                                        [curBwLimit stringValue], nil]];
        }
        NSLog(@"Starting subprocess with args: %@", args);
        task = [[NSTask launchedTaskWithLaunchPath:path arguments:args] retain];
    }
}

- (void)start:(NSString *)musicDir apiKey:(NSString *)apiKey bwLimit:(NSNumber *)bwLimit {
    if (![musicDir isEqualToString:curMusicFolder] || 
        ![apiKey isEqualToString:curApiKey] ||
        ![bwLimit isEqualToNumber:curBwLimit]) {
        curMusicFolder = [musicDir retain];
        curApiKey = [apiKey retain];
        curBwLimit = [bwLimit retain];
        [self reloadProcess];
    }
}

- (BOOL)isRunning {
    return (task != nil && [task isRunning]);
}

- (NSDictionary *)readState {
    NSString *stateFile = @"~/.djrandom.state";
    NSData *contents = [NSData dataWithContentsOfFile:[stateFile stringByExpandingTildeInPath]];
    if ([contents length] > 0) {
        NSDictionary *state = [contents yajl_JSON];
        NSLog(@"%@", state);
        return state;
    } else {
        return nil;
    }
}

@end
