import logging
import os
import json
import threading
import time

log = logging.getLogger(__name__)


class StatsDumperThread(threading.Thread):

    def __init__(self, stats):
        threading.Thread.__init__(self)
        self._stats = stats

    def run(self):
        while True:
            time.sleep(5)
            try:
                self._stats._save()
            except Exception, e:
                log.error('error saving stats: %s' % str(e))


class Stats(object):
    """Thread-safe instrumentation."""

    def __init__(self, path=None):
        if not path:
            path = os.path.expanduser('~/.djrandom.state')
        self._path = path
        self._data = {}
        self._lock = threading.Lock()
        if os.path.exists(self._path):
            try:
                with open(self._path) as fd:
                    self._data = json.load(fd)
            except:
                pass
        dumper = StatsDumperThread(self)
        dumper.setDaemon(True)
        dumper.start()

    def _save(self):
        with self._lock:
            with open(self._path, 'w') as fd:
                json.dump(self._data, fd)

    def incr(self, key, amount=1):
        with self._lock:
            if key in self._data:
                self._data[key] += amount
            else:
                self._data[key] = amount

    def set(self, key, value):
        with self._lock:
            self._data[key] = value

