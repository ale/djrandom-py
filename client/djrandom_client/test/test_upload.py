import json
import os
import mox
import unittest
import shutil
import tempfile
import urllib2
from djrandom_client import stats
from djrandom_client import throttle
from djrandom_client import upload
from djrandom_client import utils


class FileDatabaseTest(mox.MoxTestBase):

    def setUp(self):
        mox.MoxTestBase.setUp(self)
        self.tmpdir = tempfile.mkdtemp()
        self.dbpath = os.path.join(self.tmpdir, 'test.db')

    def tearDown(self):
        shutil.rmtree(self.tmpdir)
        mox.MoxTestBase.tearDown(self)

    def test_create_db(self):
        db = upload.FileDatabase(self.dbpath)
        db.close()
        self.assertTrue(os.path.exists(self.dbpath))

    def test_add_key(self):
        db = upload.FileDatabase(self.dbpath)
        db.add('/test/key')
        self.assertTrue(db.has('/test/key'))

    def test_add_key_twice(self):
        db = upload.FileDatabase(self.dbpath)
        db.add('/test/key')
        db.add('/test/key')
        self.assertTrue(db.has('/test/key'))

    def test_no_such_key(self):
        db = upload.FileDatabase(self.dbpath)
        self.assertFalse(db.has('/test/key'))


class UploadTest(mox.MoxTestBase):

    def setUp(self):
        mox.MoxTestBase.setUp(self)
        self.tmpdir = tempfile.mkdtemp()
        self.dbpath = os.path.join(self.tmpdir, 'test.db')
        self.statspath = os.path.join(self.tmpdir, 'stats')

        self.opener = self.mox.CreateMockAnything()
        self.mox.StubOutWithMock(urllib2, 'build_opener')
        urllib2.build_opener(throttle.ThrottledHTTPHandler,
                             throttle.ThrottledHTTPSHandler
                             ).AndReturn(self.opener)

        self.test_file = os.path.join(self.tmpdir, 'testfile')
        with open(self.test_file, 'w') as fd:
            fd.write('data')
        self.test_sha1 = utils.sha1_of_file(self.test_file)

    def tearDown(self):
        shutil.rmtree(self.tmpdir)
        mox.MoxTestBase.tearDown(self)

    def _create_uploader(self):
        return upload.Uploader('http://server', 'api_key',
                               db_path=self.dbpath,
                               state_path=self.statspath)

    def test_get(self):
        resp = self.mox.CreateMockAnything()
        self.opener.open(mox.IsA(urllib2.Request)).AndReturn(resp)
        resp.read().AndReturn(json.dumps({'status': True}))

        self.mox.ReplayAll()
        up = self._create_uploader()
        result = up._get('/url')
        self.assertEqual(True, result)

    def test_put(self):

        def check_request(req):
            self.assertTrue(isinstance(req, urllib2.Request))
            self.assertEquals('data', str(req.get_data()[:]))
            return True

        resp = self.mox.CreateMockAnything()
        self.opener.open(mox.Func(check_request)).AndReturn(resp)
        resp.read().AndReturn(json.dumps({'status': True}))

        self.mox.ReplayAll()

        up = self._create_uploader()
        result = up._put('/url', self.test_file)
        self.assertEqual(True, result)

    def test_upload_already_on_server(self):
        self.mox.StubOutWithMock(upload.Uploader, '_get')
        upload.Uploader._get('/check/%s' % self.test_sha1
                             ).AndReturn(True)

        self.mox.ReplayAll()

        up = self._create_uploader()
        up.upload(self.test_file)

        self.assertEquals(0, up.stats._data.get('uploaded_files', 0))

    def test_upload(self):
        self.mox.StubOutWithMock(upload.Uploader, '_get')
        upload.Uploader._get('/check/%s' % self.test_sha1
                             ).AndReturn(False)
        self.mox.StubOutWithMock(upload.Uploader, '_put')
        upload.Uploader._put('/upload/%s' % self.test_sha1,
                             self.test_file
                             ).AndReturn(True)

        self.mox.ReplayAll()

        up = self._create_uploader()
        up.upload(self.test_file)

        self.assertEquals(1, up.stats._data['uploaded_files'])

    def test_run(self):
        self.mox.StubOutWithMock(upload.Uploader, 'upload')
        upload.Uploader.upload(self.test_file)

        self.mox.ReplayAll()

        up = self._create_uploader()
        up.queue.put(self.test_file)
        up.queue.put(None)
        up.run()

        db = upload.FileDatabase(self.dbpath)
        self.assertTrue(db.has(self.test_file))

    def test_run_seen_file(self):
        db = upload.FileDatabase(self.dbpath)
        db.add(self.test_file)
        db.close()

        self.mox.StubOutWithMock(upload.Uploader, 'upload')
        self.mox.ReplayAll()

        up = self._create_uploader()
        up.queue.put(self.test_file)
        up.queue.put(None)
        up.run()

    def test_run_with_upload_error(self):
        self.mox.StubOutWithMock(upload.Uploader, 'upload')
        upload.Uploader.upload(self.test_file).AndRaise(Exception('argh!'))
        self.mox.ReplayAll()

        up = self._create_uploader()
        up.queue.put(self.test_file)
        up.queue.put(None)
        up.run()

        self.assertEquals(1, up.stats._data['errors'])

    def test_run_and_check_stats(self):
        st = self.mox.CreateMock(stats.Stats)
        self.mox.StubOutWithMock(stats, 'Stats', use_mock_anything=True)
        stats.Stats(self.statspath).AndReturn(st)

        st.set('uploading', None)
        st.set('uploading', self.test_file)

        self.mox.StubOutWithMock(upload.Uploader, 'upload')
        upload.Uploader.upload(self.test_file)

        st.set('uploading', None)
        st._save()

        self.mox.ReplayAll()

        up = self._create_uploader()
        up.stats = st
        up.queue.put(self.test_file)
        up.queue.put(None)
        up.run()


if __name__ == '__main__':
    unittest.main()
