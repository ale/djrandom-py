import os
import unittest
import shutil
import tempfile
from djrandom_client import filescan

DIR = 0
FILE = 1


class FakeQueue(object):

    def __init__(self):
        self.data = []

    def put(self, obj):
        self.data.append(obj)


class FilescanTest(unittest.TestCase):

    def setUp(self):
        self.dir = tempfile.mkdtemp()
        self._createtree([
                (DIR, 'dir1'),
                (FILE, 'dir1/file1.mp3'),
                (FILE, 'dir1/file2.txt'),
                (DIR, 'dir2'),
                (FILE, 'dir2/file3.mp3'),
                ])

    def tearDown(self):
        shutil.rmtree(self.dir)

    def _createtree(self, treedata):
        for dtype, dname in treedata:
            path = os.path.join(self.dir, dname)
            if dtype == DIR:
                os.mkdir(path)
            else:
                with open(path, 'w') as fd:
                    fd.write('data\n')

    def test_recursive_scan(self):
        queue = FakeQueue()
        n = filescan.recursive_scan(self.dir, queue)
        self.assertEquals(2, n)

        expected_files = [
            os.path.join(self.dir, 'dir1/file1.mp3'),
            os.path.join(self.dir, 'dir2/file3.mp3'),
            ]
        self.assertEquals(expected_files, queue.data)

    def test_directory_scan(self):
        queue = FakeQueue()
        n = filescan.directory_scan(self.dir, queue)
        self.assertEquals(0, n)
        self.assertEquals([], queue.data)

        queue = FakeQueue()
        n = filescan.directory_scan(self.dir + '/dir1', queue)
        self.assertEquals(1, n)
        self.assertEquals([
            os.path.join(self.dir, 'dir1/file1.mp3'),
            ], queue.data)


if __name__ == '__main__':
    unittest.main()
