import sys
import time
import threading
import unittest
import urllib2
import BaseHTTPServer
from djrandom_client import throttle


class TokenBucketTest(unittest.TestCase):

    def test_token_bucket(self):
        tb = throttle.TokenBucket(1024, 512)
        # A few tokens should be readily available.
        self.assertEquals(0, tb.consume(1024))
        # Another 2N tokens should take two seconds.
        self.assertAlmostEquals(2, tb.consume(1024), 3)

        # Now we expect tokens to be available at the
        # normal rate.
        self.assertAlmostEquals(1, tb.consume(512), 3)
        self.assertAlmostEquals(1, tb.consume(512), 3)
        self.assertAlmostEquals(1, tb.consume(512), 3)
        self.assertAlmostEquals(1, tb.consume(512), 3)

    def test_token_bucket_overburst(self):
        tb = throttle.TokenBucket(1024, 512)
        # A very large request will wait.
        self.assertEquals(18, tb.consume(10240))


class HttpSinkHandler(BaseHTTPServer.BaseHTTPRequestHandler):

    def do_POST(self):
        clen = int(self.headers.get('Content-Length', 0))
        data = self.rfile.read(clen)
        self.send_response(200)
        self.send_header('Content-type', 'text/html')
        self.end_headers()
        self.wfile.write('ok')

    do_GET = do_POST


class HttpSink(threading.Thread):

    def __init__(self, stop):
        threading.Thread.__init__(self)
        addr = ('127.0.0.1', 0)
        self.httpd = BaseHTTPServer.HTTPServer(addr, HttpSinkHandler)
        self.url = 'http://127.0.0.1:%d' % self.httpd.server_port
        self.stop = stop

    def run(self):
        while not self.stop.is_set():
            self.httpd.handle_request()


class ThrottledHttpTest(unittest.TestCase):

    def setUp(self):
        self.http_stop = threading.Event()
        self.http_server = HttpSink(self.http_stop)
        self.http_server.start()

    def tearDown(self):
        self.http_stop.set()
        urllib2.urlopen(self.http_server.url + '/quit').read()
        self.http_server.join()

    def test_rate_limit(self):
        throttle.set_rate_limit(10)

        opener = urllib2.build_opener(throttle.ThrottledHTTPHandler)
        testdata = "x" * (50 * 1024)

        req = urllib2.Request(
            self.http_server.url + '/speedtest',
            data=testdata,
            headers={'Content-Length': str(len(testdata))})
        start = time.time()
        result = opener.open(req).read()
        end = time.time()

        self.assertEquals('ok', result)

        elapsed = end - start
        print >>sys.stderr, 'elapsed: %g secs' % elapsed
        self.assertTrue(elapsed > 4.5 and elapsed < 5.5,
                        'elapsed time out of range: %g' % elapsed)

    def test_rate_limit_across_many_requests(self):
        throttle.set_rate_limit(10)

        opener = urllib2.build_opener(throttle.ThrottledHTTPHandler)
        testdata = "x" * (1024)
        n_reqs = 50

        start = time.time()
        for i in xrange(n_reqs):
            req = urllib2.Request(
                self.http_server.url + '/speedtest',
                data=testdata,
                headers={'Content-Length': str(len(testdata))})
            result = opener.open(req).read()
        end = time.time()

        self.assertEquals('ok', result)

        elapsed = end - start
        print >>sys.stderr, 'elapsed: %g secs' % elapsed
        self.assertTrue(elapsed > 4.5 and elapsed < 6.5,
                        'elapsed time out of range: %g' % elapsed)


if __name__ == '__main__':
    unittest.main()
