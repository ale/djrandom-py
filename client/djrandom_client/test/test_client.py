import mox
import os
import unittest
import shutil
import sys
import tempfile
import time
import Queue
from djrandom_client import client
from djrandom_client import daemonize
from djrandom_client import upload
from djrandom_client import throttle
from djrandom_client import utils
from djrandom_client import filescan


class EndTest(Exception):
    pass


class CompareTuple(mox.Comparator):

    def __init__(self, *ref):
        self.ref = ref

    def equals(self, rhs):
        for comparator, value in zip(self.ref, rhs):
            if comparator != value:
                return False
        return True

    def __repr__(self):
        return '(%s)' % ', '.join(str(x) for x in self.ref)


class FullScanTest(mox.MoxTestBase):

    def setUp(self):
        mox.MoxTestBase.setUp(self)
        self.queue = self.mox.CreateMock(Queue.Queue)
        self.mox.StubOutWithMock(filescan, 'recursive_scan')

    def test_run_once(self):
        filescan.recursive_scan('basedir', self.queue)
        self.queue.put(None)
        self.mox.ReplayAll()

        fs = client.FullScan('basedir', self.queue, 3600, True)
        fs.run()

    def test_run_loop(self):
        self.mox.StubOutWithMock(time, 'sleep')
        filescan.recursive_scan('basedir', self.queue)
        time.sleep(3600).AndRaise(EndTest)
        self.mox.ReplayAll()

        fs = client.FullScan('basedir', self.queue, 3600)
        self.assertRaises(EndTest, fs.run)

    def test_run_filescan_error(self):
        self.mox.StubOutWithMock(time, 'sleep')
        filescan.recursive_scan('basedir', self.queue).AndRaise(
            Exception('ahi ahi'))
        time.sleep(1800).AndRaise(EndTest)
        self.mox.ReplayAll()

        fs = client.FullScan('basedir', self.queue, 3600)
        self.assertRaises(EndTest, fs.run)


class ClientRunner(object):

    def _Run(self, args, expect_success=True):
        try:
            sys.argv = ['client'] + args
            status = client.main()
            success = not status
        except SystemExit, e:
            success = False
        self.assertEquals(
            expect_success, success,
            'execution with args: "%s" failed (status %s, expected %s)' % (
                ' '.join(args), success, expect_success))


class ClientWithConfigFileTest(ClientRunner, mox.MoxTestBase):

    def setUp(self):
        mox.MoxTestBase.setUp(self)
        self.tmpdir = tempfile.mkdtemp()
        self.old_expanduser = os.path.expanduser
        def _expanduser(p):
            if p.startswith('~/'):
                return os.path.join(self.tmpdir, p[2:])
            else:
                return p
        os.path.expanduser = _expanduser

    def tearDown(self):
        mox.MoxTestBase.tearDown(self)
        shutil.rmtree(self.tmpdir)
        os.path.expanduser = self.old_expanduser

    def test_run_options_from_config(self):
        config_file = os.path.join(self.tmpdir, '.djrandom.conf')
        with open(config_file, 'w') as fd:
            fd.write('api_key = KEY\nbwlimit = 10\n')

        self.mox.StubOutWithMock(utils, 'check_version')
        utils.check_version()

        self.mox.StubOutWithMock(daemonize, 'daemonize')
        daemonize.daemonize(mox.IgnoreArg(),
                            client.run_client,
                            CompareTuple(mox.IsA(str),
                                         mox.IsA(str),
                                         [],
                                         'KEY',
                                         None, 10, True))

        self.mox.ReplayAll()
        self._Run([])


class ClientOptionsTest(ClientRunner, mox.MoxTestBase):

    def setUp(self):
        mox.MoxTestBase.setUp(self)
        self.mox.StubOutWithMock(utils, 'read_config_defaults')
        utils.read_config_defaults(mox.IgnoreArg(), mox.IsA(str))
        self.mox.StubOutWithMock(utils, 'check_version')
        self.mox.StubOutWithMock(daemonize, 'daemonize')


    def test_client_needs_api_key(self):
        self.mox.ReplayAll()
        self._Run([], False)

    def test_too_many_arguments(self):
        self.mox.ReplayAll()
        self._Run(['--api_key=KEY', 'arg'], False)

    def test_run_default_options(self):
        utils.check_version().AndReturn(False)
        daemonize.daemonize(mox.IgnoreArg(),
                            client.run_client,
                            CompareTuple(mox.IsA(str),
                                         mox.IsA(str),
                                         [],
                                         'KEY',
                                         None, 0, True))

        self.mox.ReplayAll()
        self._Run(['--api_key=KEY'])

    def test_run_with_options(self):
        utils.check_version().AndReturn(False)
        daemonize.daemonize(mox.IgnoreArg(),
                            client.run_client,
                            CompareTuple('http://server/receiver',
                                         '/my/music',
                                         [],
                                         'KEY',
                                         True, 10))

        self.mox.ReplayAll()
        self._Run(['--api_key=KEY', '--server_url=http://server/receiver',
                   '--music_dir=/my/music', '--bwlimit=10',
                   '--once'])


class RunClientTest(mox.MoxTestBase):

    def setUp(self):
        mox.MoxTestBase.setUp(self)

    def test_run_once(self):
        self.mox.StubOutWithMock(throttle, 'set_rate_limit')
        throttle.set_rate_limit(150)

        uploader = self.mox.CreateMock(upload.Uploader)
        self.mox.StubOutWithMock(upload, 'Uploader', use_mock_anything=True)
        upload.Uploader('http://server/receiver', 'KEY', []).AndReturn(uploader)
        uploader.setDaemon(True)
        uploader.queue = 'queue!'

        fs = self.mox.CreateMock(client.FullScan)
        self.mox.StubOutWithMock(client, 'FullScan', use_mock_anything=True)
        client.FullScan('/my/music', uploader.queue, mox.IsA(int), True
                        ).AndReturn(fs)
        fs.setDaemon(True)

        uploader.start()
        fs.start()
        uploader.join()

        self.mox.ReplayAll()
        client.run_client(
            'http://server/receiver',
            '/my/music',
            [],
            'KEY',
            True,
            150)


if __name__ == '__main__':
    unittest.main()

