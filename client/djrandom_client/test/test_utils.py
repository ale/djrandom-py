import mox
import unittest
import os
import shutil
import tempfile
import urllib2
from djrandom_client import utils


class UtilsTest(mox.MoxTestBase):

    def setUp(self):
        mox.MoxTestBase.setUp(self)
        self.tmpdir = tempfile.mkdtemp()

    def tearDown(self):
        shutil.rmtree(self.tmpdir)
        mox.MoxTestBase.tearDown(self)

    def test_sha1_of_file(self):
        test_file = os.path.join(self.tmpdir, 'testfile')
        with open(test_file, 'w') as fd:
            fd.write('test\n')
        sha1 = utils.sha1_of_file(test_file)
        self.assertEquals('4e1243bd22c66e76c2ba9eddc1f91394e57f9f83', sha1)

    def test_read_config_defaults(self):
        cfg_file = os.path.join(self.tmpdir, 'config')
        with open(cfg_file, 'w') as fd:
            fd.write('''
# Test config file
var_a=a
var_b = b

  var_c  = 42
''')

        parser = self.mox.CreateMockAnything()
        parser.set_defaults(
            var_a='a', var_b='b', var_c='42')
        self.mox.ReplayAll()

        utils.read_config_defaults(parser, cfg_file)

    def test_read_config_file_with_error(self):
        cfg_file = os.path.join(self.tmpdir, 'config')
        with open(cfg_file, 'w') as fd:
            fd.write('this is not a config\n')

        parser = self.mox.CreateMockAnything()
        self.mox.ReplayAll()

        self.assertRaises(utils.SyntaxError,
                          utils.read_config_defaults,
                          parser, cfg_file)

    def test_read_config_file_missing(self):
        utils.read_config_defaults(
            None, os.path.join(self.tmpdir, 'nosuchfile'))
    

    def test_check_version_ok(self):
        resp = self.mox.CreateMockAnything()
        self.mox.StubOutWithMock(urllib2, 'urlopen')
        urllib2.urlopen(mox.IsA(str)).AndReturn(resp)
        resp.read().AndReturn("VERSION = '0.1'\n")

        self.mox.ReplayAll()
        self.assertFalse(utils.check_version())

    def test_check_version_should_upgrade(self):
        resp = self.mox.CreateMockAnything()
        self.mox.StubOutWithMock(urllib2, 'urlopen')
        urllib2.urlopen(mox.IsA(str)).AndReturn(resp)
        resp.read().AndReturn("VERSION = '9999'\n")

        self.mox.ReplayAll()
        self.assertTrue(utils.check_version())
