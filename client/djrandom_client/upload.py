import anydbm
import json
import logging
import mmap
import os
import optparse
import platform
import socket
import sqlite3
import threading
import time
import urllib2
import Queue
from djrandom_client import utils
from djrandom_client import stats
from djrandom_client import throttle
from djrandom_client import version

log = logging.getLogger(__name__)


class FileDatabase(object):
    """SQLite-backed database of seen hashes."""

    def __init__(self, dbpath=None):
        if not dbpath:
            dbpath = os.path.join(os.getenv('HOME'), '.djrandom')
        if not dbpath.endswith('.db'):
            dbpath += '.db'

        is_new = not os.path.exists(dbpath)
        self.conn = sqlite3.connect(dbpath)
        self.conn.text_factory = str

        if is_new:
            self.conn.execute(
                'create table seen (path text(1024), stamp integer, '
                'primary key (path))')
            self.conn.commit()

    def close(self):
        self.conn.close()

    def has(self, key):
        c = self.conn.cursor()
        row = c.execute(
            'select stamp from seen where path = ?', (key,)
            ).fetchone()
        if row:
            return row[0]
        else:
            return None

    def add(self, key):
        c = self.conn.cursor()
        c.execute('insert or replace into seen (path, stamp) values (?, ?)',
                  (key, int(time.time())))
        self.conn.commit()


class Uploader(threading.Thread):

    def __init__(self, server_url, api_key, excludes=[], db_path=None,
                 state_path=None):
        threading.Thread.__init__(self)
        self.api_key = api_key
        self.server_url = server_url
        self.queue = Queue.Queue(100)
        self.excludes = excludes
        self.db_path = db_path
        self.stats = stats.Stats(state_path)
        self.opener = urllib2.build_opener(throttle.ThrottledHTTPHandler,
                                           throttle.ThrottledHTTPSHandler)

        user_agent = 'djrandom_client/%s (%s %s Python/%s)' % (
            version.VERSION, platform.system(), platform.machine(),
            platform.python_version())
        self.opener.addheaders = [('User-agent', user_agent)]

        socket.setdefaulttimeout(60)

    def _get(self, url):
        req = urllib2.Request(self.server_url + url,
                              headers={'X-Key': self.api_key})
        result = json.loads(self.opener.open(req).read())
        return result['status']

    def _put(self, url, path):
        with open(path, 'r') as fd:
            filesize = os.path.getsize(path)
            filemap = mmap.mmap(fd.fileno(), filesize,
                                access=mmap.ACCESS_READ)
            try:
                req = urllib2.Request(
                    self.server_url + url, 
                    data=filemap,
                    headers={'X-Key': self.api_key,
                             'Content-Type': 'audio/mpeg',
                             'Content-Length': str(filesize)})
                result = json.loads(self.opener.open(req).read())
                return result['status']
            finally:
                filemap.close()

    def upload(self, path):
        sha1 = utils.sha1_of_file(path)
        log.info('new file: %s (%s)' % (path, sha1))
        result = self._get('/check/' + sha1)
        if result:
            log.info('%s already on server (%s)' % (path, sha1))
            return
        result = self._put('/upload/' + sha1, path)
        if not result:
            raise UploadError('server error')
        self.stats.incr('uploaded_files')
        self.stats.incr('uploaded_bytes', os.path.getsize(path))
        log.info('successfully uploaded %s (%s)' % (path, sha1))

    def _should_exclude(self, path):
        for exclude_prefix in self.excludes:
            if path.startswith(exclude_prefix):
                return True
        return False

    def run(self):
        log.debug('starting uploader thread')
        db = FileDatabase(self.db_path)
        try:
            while True:
                self.stats.set('uploading', None)
                path = self.queue.get()
                if path is None:
                    break
                if self._should_exclude(path):
                    continue
                if db.has(path):
                    continue
                self.stats.set('uploading', path)
                try:
                    self.upload(path)
                    db.add(path)
                except Exception, e:
                    log.error('error uploading %s: %s' % (path, str(e)))
                    self.stats.incr('errors')
                    self.stats.set('last_error', str(e))
                    self.stats.set('last_error_timestamp', '%i' % time.time())
        finally:
            log.debug('uploader thread exiting')
            self.stats._save()
            db.close()

    def stop(self):
        # This runs in a different thread. We can simply exit.
        pass

