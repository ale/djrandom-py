import json
import os
import shutil
import threading
import urllib
import urllib2
from errno import EINVAL, ENOENT
from stat import S_IFDIR, S_IFREG
from fuse import FUSE, Operations, LoggingMixIn



class DJAPI(object):
    """Stub for the remote HTTP API."""

    def __init__(self, server_url, api_key='fuse'):
        self._server_url = server_url
        self._api_key = api_key
        self._opener = urllib2.build_opener()

    def _create_request(self, url, *moreurlparts):
        if moreurlparts:
            url += '/' + '/'.join(map(urllib.quote, moreurlparts))
	print 'HTTP GET:', url
        return urllib2.Request(
            '%s/%s' % (self._server_url, url),
            headers={'User-Agent': 'djfuse/0.1',
                     'X-Key': self._api_key})

    def get(self, url, *moreurlparts):
        req = self._create_request(url, *urlparts)
        return json.load(self._opener.open(req))

    def download(self, sha1, path):
        print 'HTTP DOWNLOAD:', sha1
        req = self._create_request('/dl', sha1)
        resp = self._opener.open(req)
        with open(path, 'wb') as fd:
            shutil.copyfileobj(resp, fd)



class StatCache(object):

    def __init__(self, api):
        self._api = api
        self._cache = {}
        self._lock = threading.Lock()

    def _key(self, artist, album, title):
        return '%s|%s|%s' % (artist, album, title)

    def _load(self, artist, album, songs):
        with self._lock:
            dir_key = self._key(artist, album, 'DIR')
            self._cache[dir_key] = songs
            for song in songs:
                key = self._key(artist, album, song['title'])
                self._cache[key] = song

    def preload(self, artist, album):
        contents = self._api.get('/json/album/%s/%s' % (
            urllib.quote(artist), urllib.quote(album)))['songs']
        self._load(artist, album, contents)

    def get_dir(self, artist, album):
        return self.get_file(artist, album, 'DIR')

    def get_file(self, artist, album, title):
	print 'CACHE: get_file', artist, album, title
        key = self._key(artist, album, title)
        with self._lock:
            result = self._cache.get(key)
        if not result:
            self.preload(artist, album)
            with self._lock:
                result = self._cache.get(key)
        return result



class DJFS(LoggingMixIn, Operations):
    """Simple FUSE client for djrandom.

    Quite inefficient, but it shows that it can be done easily
    with the existing API, accessing files using the standard
    /artist/album/title hierarchy.
    """

    def __init__(self, server_url, cache_dir):
        if not os.path.isdir(cache_dir):
            os.makedirs(cache_dir)
        self._cache_dir = cache_dir
        self._api = DJAPI(server_url)
        self._stat_cache = StatCache(self._api)

    def destroy(self, path):
        pass

    def _find_sha1(self, artist, album, title):
        return self._stat_cache.get_file(artist, album, title)

    def _file_cache_path(self, sha1):
        return os.path.join(self._cache_dir, sha1[0], sha1)

    def _in_file_cache(self, sha1):
        return os.path.exists(self._file_cache_path(sha1))

    def _download_file(self, sha1):
        path = self._file_cache_path(sha1)
        dl_dir = os.path.dirname(path)
        if not os.path.isdir(dl_dir):
            os.makedirs(dl_dir)
        self._api.download(sha1, path)

    def _parse_path(self, path):
        parts = path[1:].split('/')
        if len(parts) > 3:
            raise OSError(EINVAL)
        if len(parts) < 3:
            parts.extend([None for x in range(3 - len(parts))])
        if parts[2] and parts[2].endswith('.mp3'):
            parts[2] = parts[2][:-4]
        return parts

    def getattr(self, path, fh=None):
        artist, album, title = self._parse_path(path)
        if title:
            stats = self._stat_cache.get_file(artist, album, title)
            if not stats:
                raise OSError(ENOENT)
            mtime = float(stats.get('uploaded_at', '0'))
            return dict(st_mode=(S_IFREG | 0444), st_nlink=1,
                        st_size=int(stats.get('size', 0)),
                        st_ctime=mtime,
                        st_mtime=mtime,
                        st_atime=mtime)
        else:
            return dict(st_mode=(S_IFDIR | 0555), st_nlink=2,
                        st_size=0, st_ctime=0, st_mtime=0, st_atime=0)

    def readdir(self, path, fh=None):
        artist, album, title = self._parse_path(path)
        if title:
            raise OSError(EINVAL)
        if not artist:
            values = self._api.get('/json/artists')['artists']
        elif not album:
            values = self._api.get('/json/albums/%s' % urllib.quote(artist))['albums']
        else:
            songs = self._stat_cache.get_dir(artist, album)
            if not songs:
                raise OSError(ENOENT)
            values = ['%s.mp3' % (x['title'] or 'UNKNOWN') for x in songs]
        return ['.', '..'] + [x.encode('utf-8') for x in values]

    def read(self, path, size, offset, fh=None):
        artist, album, title = self._parse_path(path)
        if not title:
            raise OSError(ENOENT)
        sha1 = self._find_sha1(artist, album, title)
        if not sha1:
            raise OSError(ENOENT)
        if not self._in_file_cache(sha1):
            self._download_file(sha1)
        real_path = self._file_cache_path(sha1)
        with open(real_path, 'rb') as fd:
            fd.seek(offset)
            return fd.read(size)

    # Disable unsupported calls.
    chmod = None
    chown = None
    create = None
    rename = None
    rmdir = None
    statfs = None
    symlink = None
    truncate = None
    unlink = None
    utimens = None
    write = None
    listxattr = None
    removexattr = None
    setxattr = None


def main():
    import optparse
    parser = optparse.OptionParser(usage='%Prog <MOUNTPOINT>')
    parser.add_option('--api_key')
    parser.add_option('--cache_dir',
                      default='/var/cache/djrandom/fuse')
    parser.add_option('--server_url',
                      default='http://djrandom.incal.net')
    opts, args = parser.parse_args()
    if len(args) != 1:
        parser.error('Wrong number of args')

    fuse = FUSE(DJFS(opts.server_url, opts.cache_dir),
                args[0], foreground=True, debug=True)


if __name__ == '__main__':
    main()
