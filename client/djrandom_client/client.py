#!/usr/bin/python

import logging
import optparse
import os
import platform
import signal
import sys
import threading
import time
from djrandom_client import daemonize
from djrandom_client import filescan
from djrandom_client import upload
from djrandom_client import utils
from djrandom_client import throttle

log = logging.getLogger(__name__)

# How often we rescan the filesystem looking for new files.
SCAN_DELAY = 1800


class FullScan(threading.Thread):
    """Do a recursive directory scan when starting."""

    def __init__(self, basedir, queue, base_delay, exit_when_done=False):
        threading.Thread.__init__(self)
        self.basedir = basedir
        self.base_delay = base_delay
        self.queue = queue
        self.exit_when_done = exit_when_done

    def run(self):
        while True:
            delay = self.base_delay
            log.debug('starting file scan')
            try:
                filescan.recursive_scan(self.basedir, self.queue)
            except Exception, e:
                log.error('Error in file scan: %s' % e)
                # Retry 30 minutes after an error.
                delay = 1800
            if self.exit_when_done:
                self.queue.put(None)
                break
            time.sleep(delay)


def run_client(server_url, music_dir, excludes, api_key, run_once, bwlimit):
    log.debug('settings: server=%s, music_dir=%s, api_key=%s, '
              'bwlimit=%s' % (
        server_url, music_dir, api_key, bwlimit))

    # Warn if we're running without a bandwidth limit.
    bwlimit = int(bwlimit)
    if bwlimit <= 0:
        log.warn('Running without a bandwidth limit!')
    else:
        throttle.set_rate_limit(bwlimit)

    # Warn on this condition, but don't die -- the directory might exist later!
    music_dir = os.path.realpath(os.path.expanduser(music_dir))
    if not os.path.isdir(music_dir):
        log.error('The music_dir you specified does not exist')

    # Compute absolute exclude paths.
    abs_excludes = []
    for exclude_prefix in excludes:
        if not exclude_prefix.startswith('/'):
            exclude_prefix = os.path.join(music_dir, exclude_prefix)
        if not exclude_prefix.endswith('/'):
            exclude_prefix += '/'
        abs_excludes.append(os.path.realpath(exclude_prefix))

    upl = upload.Uploader(server_url.rstrip('/'), api_key, abs_excludes)
    upl.setDaemon(True)

    # Start the full filesystem scan in the background.
    scan = FullScan(music_dir, upl.queue, SCAN_DELAY, run_once)
    scan.setDaemon(True)

    if not run_once:
        # Run at a lower priority.
        os.nice(10)
        if os.path.exists('/usr/bin/ionice'):
            # Set 'idle' I/O scheduling class, we won't disturb other programs.
            os.system('/usr/bin/ionice -c 3 -p %d' % os.getpid())

        # Install termination signal handlers.
        def _cleanup(signum, frame):
            log.info('got signal %d, exiting...' % signum)
            upl.stop()
            sys.exit(0)
        signal.signal(signal.SIGINT, _cleanup)
        signal.signal(signal.SIGTERM, _cleanup)

    upl.start()
    scan.start()

    if run_once:
        upl.join()
    else:
        # It turns out that, even if we set up signal handlers in this
        # same main thread, they won't be executed if we're blocking on
        # a mutex (such as 'upl.join()' for example)... so we do this
        # silly idle loop in the main thread just to ensure that we
        # catch SIGTERM and SIGINT.
        while True:
            time.sleep(3600)


def main():
    parser = optparse.OptionParser(usage='%prog [<OPTIONS>]')
    parser.add_option('--api_key',
                      help='Your API key')
    parser.add_option('--once', action='store_true',
                      help='Scan music_dir once and then exit')
    parser.add_option('--music_dir',
                      default='~/Music',
                      help='Path to your music directory')
    parser.add_option('--exclude',
                      action='append', default=[],
                      help='Exclude certain paths from scanning')
    parser.add_option('--server_url',
                      default='https://djrandom.incal.net/receiver',
                      help='URL to the API endpoint')
    parser.add_option('--bwlimit', type='int', default=0,
                      help='Upload bandwidth limit, kilobytes/s (default: '
                      'unlimited)')
    parser.add_option('--skip_version_check', action='store_true')
    daemonize.add_standard_options(parser)
    utils.read_config_defaults(
        parser, os.path.expanduser('~/.djrandom.conf'))
    parser.set_default(
        'pidfile', os.path.expanduser('~/.djrandom.pid'))
    opts, args = parser.parse_args()
    if not opts.api_key:
        parser.error('You must specify an API Key')

    if args:
        parser.error('Too many arguments')

    # Perform a version check.
    if not opts.skip_version_check and utils.check_version():
        print >>sys.stderr, 'A new release is available! Please update.'

    daemonize.daemonize(opts, run_client, 
                        (opts.server_url, opts.music_dir, opts.exclude, 
                         opts.api_key, opts.once, opts.bwlimit))


if __name__ == '__main__':
    main()
