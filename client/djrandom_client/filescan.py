import os

DIR_BLACKLIST = set(['.AppleDouble'])


def recursive_scan(basedir, queue):
    n = 0
    for root, dirs, files in os.walk(basedir, topdown=True,
                                     followlinks=True):
        prune_dirs = [x for x in dirs
                      if (x in DIR_BLACKLIST or x.startswith('.'))]
        for pdir in prune_dirs:
            dirs.remove(pdir)
        for filename in files:
            if (filename.lower().endswith('.mp3') and
                not filename.startswith('.')):
                path = os.path.join(root, filename)
                queue.put(path)
                n += 1
    return n


def directory_scan(directory, queue):
    n = 0
    for filename in os.listdir(directory):
        if filename.lower().endswith('.mp3'):
            path = os.path.join(directory, filename)
            if os.path.isfile(path):
                queue.put(path)
                n += 1
    return n

