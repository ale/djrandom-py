import logging
import httplib
import socket
import threading
import time
import urllib2

log = logging.getLogger(__name__)


class TokenBucket(object):
    """An implementation of the token bucket algorithm.

    Source: http://code.activestate.com/recipes/511490/ (thread-safe).
    """

    def __init__(self, tokens, fill_rate):
        """Cosntructor.

        tokens is the total tokens in the bucket. fill_rate is the
        rate in tokens/second that the bucket will be refilled.
        """
        self.capacity = float(tokens)
        self._tokens = float(tokens)
        self.fill_rate = float(fill_rate)
        self.timestamp = time.time()
        self.lock = threading.RLock()

    def consume(self, tokens):
        """Consume tokens from the bucket. 

        Returns 0 if there were sufficient tokens, otherwise the
        expected time until enough tokens become available.
        """
        with self.lock:
            tokens = max(tokens, self.tokens)
            expected_time = (tokens - self.tokens) / self.fill_rate
            if expected_time <= 0:
                self._tokens -= tokens
            return max(0, expected_time)

    @property
    def tokens(self):
        with self.lock:
            if self._tokens < self.capacity:
                now = time.time()
                delta = self.fill_rate * (now - self.timestamp)
                self._tokens = min(self.capacity, self._tokens + delta)
                self.timestamp = now
            value = self._tokens
            return value


# We maintain a single process-wide TokenBucket for rate-limiting
# HTTP uploads.  If set_rate_limit() is not called, no bandwidth
# limit will be applied.
_bucket = None

def set_rate_limit(kbps):
    global _bucket
    log.info('setting upload bandwidth limit to %d KB/s' % kbps)
    bps = 1024 * kbps
    _bucket = TokenBucket(2 * bps, bps)


class ThrottledHTTPConnectionMixin(object):

    BLOCK_SIZE = 4096

    def throttled_send(self, str):
        if self.sock is None:
            if self.auto_open:
                self.connect()
            else:
                raise httplib.NotConnected()

        try:
            tot = len(str)
            for off in xrange(0, tot, self.BLOCK_SIZE):
                n = min(self.BLOCK_SIZE, tot - off)
                chunk = str[off:off + n]
                if _bucket:
                    wait_time = _bucket.consume(n)
                    while wait_time > 0:
                        time.sleep(wait_time)
                        wait_time = _bucket.consume(n)
                self.sock.sendall(chunk)
        except socket.error, v:
            if v[0] == 32:      # Broken pipe
                self.close()
            raise


class ThrottledHTTPSConnection(httplib.HTTPSConnection,
                               ThrottledHTTPConnectionMixin):

    def send(self, str):
        return self.throttled_send(str)


class ThrottledHTTPConnection(httplib.HTTPConnection,
                              ThrottledHTTPConnectionMixin):

    def send(self, str):
        return self.throttled_send(str)


class ThrottledHTTPHandler(urllib2.HTTPHandler):

    def http_open(self, req):
        return self.do_open(ThrottledHTTPConnection, req)


class ThrottledHTTPSHandler(urllib2.HTTPSHandler):

    def https_open(self, req):
        return self.do_open(ThrottledHTTPSConnection, req)


