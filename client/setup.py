#!/usr/bin/python

# Read version from version.py.
import sys, os
sys.path.insert(0, os.path.dirname(__file__))
from djrandom_client import version

from setuptools import setup, find_packages
import platform
if platform.system() == 'Darwin':
    extra_options = {
      'app': ['djrandom_client/client.py'],
      'setup_requires': ['py2app']
    }
else:
    extra_options = {}

setup(
  name="djrandom-client",
  version=version.VERSION,
  description="DJ:Random client",
  author="ale",
  author_email="ale@incal.net",
  url="http://git.autistici.org/git/djrandom.git",
  zip_safe=True,
  packages=find_packages(),
  entry_points={
    "console_scripts": [
      "djrandom-client = djrandom_client.client:main",
    ],
  },
  **extra_options
  )

