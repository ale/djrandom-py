
Client Installation
===================

1. Get an API key. They can be requested from the user details page,
   accessible by clicking on your email address at the top right of
   the main page.


Install from source
-------------------

2. Clone the Git repository:

    $ git clone http://git.autistici.org/djrandom.git

3. Install the client. Python will automatically download all the
   required dependencies:

    $ cd client
    $ sudo python setup.py install

4. Configure it. Add the following lines to ~/.djrandom.conf:

    music_dir = PATH_TO_MUSIC_DIR
    api_key = API_KEY

   replacing PATH_TO_MUSIC_DIR with the full path to the directory
   holding your music files, and API_KEY with your own API key.


Install from package
--------------------

2. Client packages are available:

   OSX:
   - http://git.autistici.org/p/djrandom/dist/DJRandomUploader.zip

   Linux (Debian/Ubuntu):
   - Add the following repository to your list of APT sources (you
     can simply create /etc/apt/sources.list.d/djrandom.list for
     that if your apt is recent enough):

       deb http://git.autistici.org/p/djrandom/debian unstable main

     Install the repository GPG key with:

       $ wget -O- http://git.autistici.org/p/djrandom/debian/repo.key \
         | sudo apt-key add -

     Then update and install the djrandom-client package:

       $ sudo apt-get update
       $ sudo apt-get install djrandom-client


Running the client
------------------

5. It's a normal well-behaved daemon:

    $ djrandom-client

   The program will fork into the background and upload your music.
   Logs will be sent to syslog (you can use --debug and other
   options to figure out what's going on in case of trouble).


